% Evolutionary Algorithm
% -----------------------------
% Author: Jeremias Seitz
% Modified: Tonci Novkovic
% Modified: Maria Veiga
% -----------------------------
% This is the main frame, where the user can run the simulation

%------------------------------
% INITIALIZATION
%------------------------------
clear all;

initPars = init_m(); %load properties of the problem
algo = algo_m(); %load properties of the algorithm

%l=1;

obstacles = initPars.obstacles;

%generate the structures that hold the organisms
organism = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
mutate = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
elite = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
crossover = struct('gene', {}, 'coordVec', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});

% probability density function used to calculate the probability of
% choosing a certain organism for mutation and cross over (first organism 
% has the highest probability)
pdf_mut_mix = pdf(initPars.nPopulation);



% INITIAL POPULATION
generation = 1;                 
pStart = tic;
    i = 1;
    while i <= initPars.nPopulation
        r = rand;
        pdf_init = ones(1,initPars.nCubeMax-initPars.nCubeMin+1)*1/(initPars.nCubeMax-initPars.nCubeMin+1);
        nCube = find(r <= cumsum(pdf_init),1)+initPars.nCubeMin-1;
        
        %generate sequence
        gene = encode(nCube,initPars.maxOps,algo.useObstacles,obstacles);
        organism(generation,i)=build_m(gene,obstacles,initPars.externalLoad);
        [allright,maxStress,maxDisplacement,num_con] = checkstability_m(organism(generation,i).gene,organism(generation,i).coordVec,initPars.externalLoad,initPars.destination,algo.useObstacles,obstacles);
        %[allright,maxStress,maxDisplacement,num_con] = checkstability(organism(generation,i).bdir,organism(generation,i).startVec,initPars.externalLoad,initPars.externalLoadPosition,algo.useObstacles,obstacles);
        organism(generation,i).maxStress = maxStress;
        organism(generation,i).maxDisplacement = maxDisplacement;
        organism(generation,i).numCon = num_con;
        organism(generation,i).gene = gene;
%         if checkexisting(organism(generation,:),i)
%             continue;
%         end
         if allright ~= 1
             continue;
         end
        i = i+1;
    end
%end
 orgs(generation,:) = checkfitness_m(initPars.nPopulation,organism(generation,:),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
 f = fitnessfunction_m(organism(generation,1),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
 pEnd = toc(pStart);
 disp([num2str(generation),'/',num2str(initPars.maxGen),'   ',num2str(f,'%7.4f'), '    Time: ', num2str(pEnd,'%6.2f')]);

 %render_m(organism(generation,1),initPars.destination);
 generation = generation + 1;
 iTime = 0;
 mTime = 0;
 cTime = 0;

 % % ELITE, MUTATION, CROSS OVER
 while f < 150 && generation<=initPars.maxGen
     iStart = tic;
     prev_gen(1:initPars.nPopulation) = organism(generation-1,:);
%     
     %externalLoad = generation .* 0.03 .* [-1 1 -1]; 
     %what is this???     

     % Elite
     elite(1:initPars.nElite) = prev_gen(1:initPars.nElite);

     % Mutation
     nMutation = initPars.nMutate;
     mStart = tic;
%      if useParallel
%          parfor i = 1:initPars.nMutation
%              mut_rand = rand;
%              mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
%              mutate(i) = mutation(prev_gen(mutOrg),useStability,externalLoad,externalLoadPosition,useObstacles,obstacles);
%          end
%      else
         i = 1;
         while i <= nMutation
             mut_rand = rand;
             mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
             mutatedgene=mutation_m(prev_gen(mutOrg).gene);
             mutate(i) = build_m(mutatedgene,obstacles,initPars.externalLoad);
             [allright,maxStress,maxDisplacement,num_con] = checkstability_m(mutate(i).gene,mutate(i).coordVec,initPars.externalLoad,initPars.destination,algo.useObstacles,obstacles);
             if allright ~= 1
                continue;
             end
             i = i+1;
         end

     mEnd = toc(mStart);
     mTime = mTime + mEnd;

     % Cross over
     cStart = tic;
        j = 1;
        while j <= initPars.nCrossover
            A_rand = rand;
            B_rand = rand;
            objA = find(A_rand <= cumsum(pdf_mut_mix),1);        % evolving
            objB = find(B_rand <= cumsum(pdf_mut_mix),1);        % providing elemets to A
            covergene = mix_m(prev_gen(objA).gene,prev_gen(objB).gene);
            crossover(j)=build_m(covergene,obstacles,initPars.externalLoad);
                [stable,maxStress,maxDisplacement,numCon] = checkstability_m(crossover(j).gene,crossover(j).coordVec,initPars.externalLoad,initPars.destination,algo.useObstacles,obstacles);
            if stable ~= 1
             continue;
            end
            j = j+1;
        end
    
    cEnd = toc(cStart);
    cTime = cTime + cEnd;
    crossover(1:initPars.nCrossover) = prev_gen(1:initPars.nCrossover);
    organism(generation,:) = [elite, mutate, crossover];
    organism(generation,:) = checkfitness_m(initPars.nPopulation,organism(generation,:),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
    organism(generation,:) = organism(generation,:);
    f = fitnessfunction_m(organism(generation,1),initPars.destination,algo.useDeflection,algo.useStress,algo.useCubes,algo.useConnections);
      
    iEnd = toc(iStart);
    iTime = iTime + iEnd;
    cEnd=1;
    disp([num2str(generation),'/',num2str(initPars.maxGen),'   ',num2str(f,'%7.4f'),'    Mutation: ',num2str(mEnd,'%6.2f'),' Crossover: ',num2str(cEnd,'%6.2f'),' Iteration: ',num2str(iEnd,'%6.2f')]);
    generation = generation + 1;
%     
     if generation > initPars.maxGen
         break;
     end
     
 end
 
 
 disp (['Total Run Time: ',num2str(iTime,'%6.2f'),'   Mutation: ',num2str(mTime,'%6.2f'),'   Cross over: ',num2str(cTime,'%6.2f')]);
 %disp ('Fittest gene:');
 
 
 %organism(generation-1,1).gene
 %make the movie
 makeEvolutionMovie_m(organism,initPars,initPars.destination,obstacles)