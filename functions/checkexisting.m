function exist = checkexisting (generation,current)
% INPUT:
% generation - generation in which to check for existing
% current - current organism for which we want to check if it already exists
% in the generation
% OUTPUT:
% 1 - organism "current" exists in "generation"
% 0 - organism "current" doesn't exist in "generation

startVecA = generation(current).startVec;
parentVecA = generation(current).parentVec;
bdirA = generation(current).bdir;

for i = 1:current-1
    
    check = 0;
    exist = 0;
    startVecB = generation(i).startVec;
    parentVecB = generation(i).parentVec;
    bdirB = generation(i).bdir;
    
    if size(parentVecA,1) == size(parentVecB,1)
        if ~isequal(parentVecA,parentVecB)
            continue;
        end
        if ~isequal(startVecA,startVecB)
            continue;
        end
        for j = 1:size(parentVecA,1)
            if ~isequal(bdirA{j}, bdirB{j})
                continue;
            else
                check = check + 1;
            end
        end
        if check == size(parentVecA,1)
            exist = 1;
            return;
        end
    end
    
end
exist = 0;

end