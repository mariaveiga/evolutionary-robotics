function [bdir_new, flag] = addBranch(nCube)

% bdir (short for branch direction) is the encoding of the structure (so to
% speak the 'DNA'). it shows where the respectively next cube will be placed
% depending on the current one. dir stores the unit vector in the cartesian
% system.
dir = zeros(nCube,3);
xCoord = nCube+1;
yCoord = nCube+1;
zCoord = nCube+1;
% locMat describes the location of each cube. 1 -> space occupied, 0 ->
% space free. it is used to check and prevent collision. because we start
% at small cube numbers this method is faster than using the location
% vectors of each cube to check if they overlap
locMat = zeros(nCube*2+1,nCube*2+1,nCube*2+1);
locMat(xCoord,yCoord,zCoord) = 1;
iter = 1;
check = 0;
flag = 0;

%the starting cube is always already defined, so there is no need to create
%it again
while iter <= nCube
    
    if check < 100
        switch randi(6)
            case 1
                xCoord = xCoord + 1;
                if locMat(xCoord,yCoord,zCoord)
                    xCoord = xCoord - 1;
                    check = check + 1;
                    continue;
                end
                dir(iter,1) = 1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 2
                xCoord = xCoord - 1;
                if locMat(xCoord,yCoord,zCoord)
                    xCoord = xCoord + 1;
                    check = check + 1;
                    continue;
                end
                dir(iter,1) = -1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 3
                yCoord = yCoord + 1;
                if locMat(xCoord,yCoord,zCoord)
                    yCoord = yCoord - 1;
                    check = check + 1;
                    continue;
                end
                dir(iter,2) = 1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 4
                yCoord = yCoord - 1;
                if locMat(xCoord,yCoord,zCoord)
                    yCoord = yCoord + 1;
                    check = check + 1;
                    continue;
                end
                dir(iter,2) = -1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 5
                zCoord = zCoord + 1;
                if locMat(xCoord,yCoord,zCoord)
                    zCoord = zCoord - 1;
                    check = check + 1;
                    continue;
                end
                dir(iter,3) = 1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 6
                zCoord = zCoord - 1;
                if locMat(xCoord,yCoord,zCoord)
                    zCoord = zCoord + 1;
                    check = check + 1;
                    continue;
                end
                dir(iter,3) = -1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            otherwise
                return;
        end
    else
        bdir_new = 0;
        flag = 1;
        return;
    end
end

bdir_new = dir;

end