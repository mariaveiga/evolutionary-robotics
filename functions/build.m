function sequence = build (organism)
startVec = organism.startVec;
parentVec = organism.parentVec;
bdir = organism.bdir;

% Number of cubes 
nElements = sum(cellfun('size',bdir,1))+1;

% Find coordinates of each element
coordVec = zeros(nElements,3);
coordVec(1,:) = [0 0 0]; 
index = 2;
for j = 1:size(startVec,1)
    coordinates = startVec(j,:);
    for k = 1:size(bdir{j},1)
        coordinates = coordinates + bdir{j}(k,:);
        coordVec(index,:) = coordinates;        
        index = index + 1;
    end
end

coordVec(:,2) = [];
[minCent, minIdx] = group (coordVec);


% % Find 3D matrix that has 1 where each cube is placed
% sizeX = max(coordVec(:,1)) - min(coordVec(:,1));
% sizeY = max(coordVec(:,2)) - min(coordVec(:,2));
% sizeZ = max(coordVec(:,3)) - min(coordVec(:,3));
% locMat = zeros(sizeX+1,sizeY+1,sizeZ+1); 
% shift = [1-min(coordVec(:,1)) 1-min(coordVec(:,2)) 1-min(coordVec(:,3))];
% coordFit(:,1) = coordVec(:,1) + shift(1);
% coordFit(:,2) = coordVec(:,2) + shift(2);
% coordFit(:,3) = coordVec(:,3) + shift(3);
% for l = 1:nElements
%     locMat(coordFit(l,1),coordFit(l,2),coordFit(l,3)) = l;
% end
% 
% % Find number of connections and connection matrix
% nConnection = zeros(nElements,1);
% conMat = zeros(6*nElements,3);
% for n = 1:nElements
%     eC = coordFit(n,:); 
%     if eC(1) > 1 && locMat(eC(1)-1,eC(2),eC(3)) ~= 0         
%         elemB = locMat(eC(1)-1,eC(2),eC(3));
%         nConnection(n,1) = nConnection(n,1) + 1;
%         nConnection(elemB,1) = nConnection(elemB,1) + 1;
%         conMat(sum(nConnection)/2,:) = [n,elemB,1];     
%     end
%     if eC(2) > 1 && locMat(eC(1),eC(2)-1,eC(3)) ~= 0         
%         elemB = locMat(eC(1),eC(2)-1,eC(3));
%         nConnection(n,1) = nConnection(n,1) + 1;
%         nConnection(elemB,1) = nConnection(elemB,1) + 1;
%         conMat(sum(nConnection)/2,:) = [n,elemB,2];       
%     end
%     if eC(3) > 1 && locMat(eC(1),eC(2),eC(3)-1) ~= 0 
%         elemB = locMat(eC(1),eC(2),eC(3)-1);
%         nConnection(n,1) = nConnection(n,1) + 1;
%         nConnection(elemB,1) = nConnection(elemB,1) + 1;
%         conMat(sum(nConnection)/2,:) = [n,elemB,3];
%     end
% end
% conMat = conMat(1:sum(nConnection)/2,:); 
%
% ground = find(coordVec(:,3)==0);
% groundCoord = [];
% for i = 1:size(ground,1)
%     groundCoord = [groundCoord; coordVec(ground(i),:)];
% end
% sequence{1} = groundCoord;
% 
% 
% iter = 1;
% 
% while iter <= nElements
%     
%     
% end

end
