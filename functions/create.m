function organism = create(nCube,useObstacles,obstacles)
%--------------------------------------------------------------------------
% this function creates a random structure of a certain amount of building
% blocks. in this case the building blocks are simple cubes. a cube
% therefore has six possible ways to connect to an antecedent element. to
% prevent overlapping geometry, the location of each element is stored such
% that no further element can be placed at the same location.
% the organism consists of three different parts:
% 
% bdir (branch direction) is the encoding of the structure (so to speak the
% 'DNA'). it shows where the respectively next cube will be placed
% depending on the current one. dir stores the unit vector in the cartesian
% system. each branch has its own cell in bdir with the branch number as
% its index. this variable represents the mutating and mixing component of
% the organisms, while the other two get adjusted to whatever change the
% direction vectors undergo.
%
% startVec (start vector) stores the starting point of each branch, so the
% spatial alignment of all elements is clearly defined.
%
% parentVec (parent vector) shows the relationships between branches. every
% branch has a distinct number, which is represented by the indices numbers
% of the parent vector. if a sidebranch connects to another branch, this
% other branch is by definition its parent and as a result the number of
% this branch is stored at the index, which is reserved to the sidebranch.
% this is useful, whenever transformations take place and it is important
% to know which branches also have to be modified.
%
% input:
% nCube - number of elements the initial structure should be composed of
% filename - name of how the organsism should be saved after creation
%--------------------------------------------------------------------------

% since this function just creates the initial branches, there is just one
% start vector coordinate and one element in the parent vector. note: the
% main branch has no parents and therefore a zero is stored.
nBranch = 1;        % number of branches
startVec = [0 0 0]; % origin coordinates
parentVec = 0;      % no parent
maxStress = 0; % maximal stress in the structure
maxDisplacement = 0;
num_con = 0;

dir = zeros(nCube-1,3); % stores direction vectors
xCoord = nCube;
yCoord = nCube;
zCoord = nCube;
% locMat describes the location of each cube. 1 = space occupied, 0 = space
% free. it is used to check and prevent collision. this method is faster
% than using the location vectors of each cube to check if they overlap,
% because at this stage, there are just a few elements.
locMat = zeros(nCube*2+1,nCube*2+1,nCube*2+1); 
locMat(nCube,nCube,nCube) = 1;
iter = 1;
flag = 0;

% the starting cube is already predefined, so there has to be added one
% less cube.
control = 0;
while iter <= nCube-1
    flag = 0;
    control = control + 1;
    if control < 200
        switch randi(6) %add cubes randomly
            case 1
                xCoord = xCoord + 1;
                if locMat(xCoord,yCoord,zCoord)
                    xCoord = xCoord - 1;
                    continue;
                end
                if useObstacles
                    for e = 1:size(obstacles,1)
                        if isequal([xCoord-nCube, yCoord-nCube, zCoord-nCube],obstacles(e,:))
                            flag = 1;
                            break;
                        end
                    end
                    if flag
                        xCoord = xCoord - 1;
                        continue;
                    end
                end
                dir(iter,1) = 1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 2
                xCoord = xCoord - 1;
                if locMat(xCoord,yCoord,zCoord)
                    xCoord = xCoord + 1;
                    continue;
                end
                if useObstacles
                    for e = 1:size(obstacles,1)
                        if isequal([xCoord-nCube, yCoord-nCube, zCoord-nCube],obstacles(e,:))
                            flag = 1;
                            break;
                        end
                    end
                    if flag
                        xCoord = xCoord + 1;
                        continue;
                    end
                end
                dir(iter,1) = -1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 3
                yCoord = yCoord + 1;
                if locMat(xCoord,yCoord,zCoord)
                    yCoord = yCoord - 1;
                    continue;
                end
                if useObstacles
                    for e = 1:size(obstacles,1)
                        if isequal([xCoord-nCube, yCoord-nCube, zCoord-nCube],obstacles(e,:))
                            flag = 1;
                            break;
                        end
                    end
                    if flag
                        yCoord = yCoord - 1;
                        continue;
                    end
                end
                dir(iter,2) = 1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 4
                yCoord = yCoord - 1;
                if locMat(xCoord,yCoord,zCoord)
                    yCoord = yCoord + 1;
                    continue;
                end
                if useObstacles
                    for e = 1:size(obstacles,1)
                        if isequal([xCoord-nCube, yCoord-nCube, zCoord-nCube],obstacles(e,:))
                            flag = 1;
                            break;
                        end
                    end
                    if flag
                        yCoord = yCoord + 1;
                        continue;
                    end
                end
                dir(iter,2) = -1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 5
                zCoord = zCoord + 1;
                if locMat(xCoord,yCoord,zCoord)
                    zCoord = zCoord - 1;
                    continue;
                end
                if useObstacles
                    for e = 1:size(obstacles,1)
                        if isequal([xCoord-nCube, yCoord-nCube, zCoord-nCube],obstacles(e,:))
                            flag = 1;
                            break;
                        end
                    end
                    if flag
                        zCoord = zCoord - 1;
                        continue;
                    end
                end
                dir(iter,3) = 1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            case 6
                zCoord = zCoord - 1;
                if locMat(xCoord,yCoord,zCoord)
                    zCoord = zCoord + 1;
                    continue;
                end
                if useObstacles
                    for e = 1:size(obstacles,1)
                        if isequal([xCoord-nCube, yCoord-nCube, zCoord-nCube],obstacles(e,:))
                            flag = 1;
                            break;
                        end
                    end
                    if flag
                        zCoord = zCoord + 1;
                        continue;
                    end
                end
                dir(iter,3) = -1;
                locMat(xCoord,yCoord,zCoord) = 1;
                iter = iter + 1;
            otherwise
                return;
        end
    else
        xCoord = nCube;
        yCoord = nCube;
        zCoord = nCube;
        locMat = zeros(nCube*2+1,nCube*2+1,nCube*2+1);
        locMat(nCube,nCube,nCube) = 1;
        iter = 1;
        control = 0;
    end

end

bdir{nBranch} = dir;

organism = struct('parentVec', parentVec, 'startVec', startVec, 'bdir', {bdir}, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', num_con);

end