function makeMovie(organism,nFrames,destination,useObstacles,obstacles)

delete('movieFrames/vid*');
renderVideo(organism(1,1),destination,useObstacles,obstacles);
camerapos = campos;
camviewangle = camva;
camtarg = camtarget;
camupvector = camup;
clf;

for generation = 1:nFrames-1

        renderVideo(organism(generation,1),destination,useObstacles,obstacles);
        campos(camerapos);
        camva(camviewangle);
        camtarget(camtarg);
        camup(camupvector);
        drawnow;
        saveas(gcf,['movieFrames/vid_' num2str(generation,'%03d') '.jpg']);
        close;

end

renderVideo(organism(generation+1,1),destination,useObstacles,obstacles);
campos(camerapos);
camva(camviewangle);
camtarget(camtarg);
camup(camupvector);
drawnow;
angleStep = 5;
for angle = 1:360/angleStep
    camorbit(angleStep,0);
    saveas(gcf,['movieFrames/vid_' num2str(generation+angle,'%03d') '.jpg']);
end

for hold = 1:1
    saveas(gcf,['movieFrames/vid_' num2str(generation+angle+hold,'%03d') '.jpg']);
end
close;
end