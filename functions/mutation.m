function mutate = mutation(organism,useStability,externalLoad,externalLoadPosition,useObstacles,obstacles)

%define rotation matrices, the rotation angle is either +pi/2 or -pi/2.
%they are used at mutation type 3.
rotXpos = [1 0 0;0 0 -1;0 1 0];
rotXneg = [1 0 0;0 0 1;0 -1 0];
rotYpos = [0 0 1;0 1 0;-1 0 0];
rotYneg = [0 0 -1;0 1 0;1 0 0];
rotZpos = [0 -1 0;1 0 0;0 0 1];
rotZneg = [0 1 0;-1 0 0;0 0 1];
RotationMatrix = [rotXpos';rotXneg';rotYpos';rotYneg';rotZpos';rotZneg'];
maxStress = 0; % maximal stress in the structure
maxDisplacement = 0;
numCon = 0;
% UPDATE 11.7. added displacement

while true % gets broken once a mutation is successful
    
    startVec = organism.startVec;
    parentVec = organism.parentVec;
    bdir = organism.bdir;
    nBranch = size(parentVec,1);
    mSize = sum(cellfun('size',bdir,1));
    
    if mSize == 0
        mutate = organism;
        return;
    end
    mSpot = randi(mSize); % the spot in the "genome" that gets mutated.
    % note: the mutation targets only the direction vectors and not the
    % elements itself (except when adding branches). as a result the
    % following element - the one influenced from the direction vector -
    % gets changed. so if for example mSpot is 1, then the second element
    % gets modified. the element at the origin can't be altered, because it
    % will be the one connected to the robot.
    checkingbranch = 1;
    bNumber = 1;
    
    while checkingbranch % determine the branch number on which an elements is mutated
        
        if mSpot > size(bdir{bNumber},1)
            mSpot = mSpot - size(bdir{bNumber},1);
            bNumber = bNumber + 1;
        else
            checkingbranch = 0;
        end
        
    end
    
    % toChange stores every branch that possibly has to be modified
    % depending on the mutation -> every branch whose parent is being
    % mutated
    toChange = find(parentVec == bNumber);
    
    % if a branch connects to the mutated branch ahead of the mutation
    % spot, no modification has to be made, because it feels no change and
    % therefore the particular branch can be ignored
    if any(toChange)
        ignSize = mSpot - 1; % the number of direction vectors up to which
        % connecting branches can be ignored for adjusting
        difference = false(size(toChange,1),1);
        % all start vectors of are getting compared to the elements ahead of
        % the mutation spot. if they match, no modification has to be made on
        % the corresponding branch.
        ignStart = startVec(bNumber,:);
        for ign=0:ignSize
            
            ignoreVec = ignStart + sum(bdir{bNumber}(1:ign,:),1);
            for k=1:size(toChange,1)
                if startVec(toChange(k),:) == ignoreVec
                    difference(k) = true;
                end
            end
            
        end
        toChange(difference) = []; %the ignored branches get sorted out
    end;
    %--------------------------------------------------------------------------
    %the mutation type is determined totally random. currently, there are five
    %different mutation types implemented, each with equal probability. In the
    %case of modifying a branch with children, the parent vector indicates
    %which branches will have to be modified to accomplish the proper mutation.
    %--------------------------------------------------------------------------
    
    mutRand = rand;
    mutpdf = ones(1,5)*0.2;
    mutMode = find(mutRand <= cumsum(mutpdf),1);
    
    switch mutMode
            %----------------------------------------------------------------------
        case 1 % duplication of single elements
            %----------------------------------------------------------------------
            %first the mutating branch gets changed -> the 'actual' mutation
            dupElem = bdir{bNumber}(mSpot,:); % direction vector of the duplicated element
            bdir{bNumber} = [bdir{bNumber}(1:mSpot,:);bdir{bNumber}(mSpot:end,:)];
            
            %second all the children have to be modified accordingly
            while any(toChange)
                for child=1:size(toChange,1)
                    startVec(toChange(child),:) = startVec(toChange(child),:) + dupElem;
                end
                toChange = find(ismember(parentVec, toChange)); % finds the children of children
            end
            
            if useStability
                [stable,maxStress,maxDisplacement,numCon] = checkstability(bdir,startVec,externalLoad,externalLoadPosition,useObstacles,obstacles);
                if stable ~= 1
                    % disp(stable);  % for debugging
                    continue;
                end
            else
                if checkcollision(bdir,startVec) == false
                    continue;
                end
            end
            
            mutate = struct('parentVec', parentVec, 'startVec', startVec, 'bdir', {bdir}, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', numCon);
            break; % mutation successful
            
            %----------------------------------------------------------------------
        case 2 % growing an element
            %----------------------------------------------------------------------
            bSize = 1;
            [branch, flag] = addBranch(bSize);
            if flag
                continue;
            else
                bdir{bNumber}(end+1:end+bSize,:) = branch;
            end
            % UPDATE 8.7. added check for adding branch
            
            if useStability
                [stable,maxStress,maxDisplacement,numCon] = checkstability(bdir,startVec,externalLoad,externalLoadPosition,useObstacles,obstacles);
                if stable ~= 1
                    % disp(stable);  % for debugging
                    continue;
                end
            else
                if checkcollision(bdir,startVec) == false
                    continue;
                end
            end
            
            mutate = struct('parentVec', parentVec, 'startVec', startVec, 'bdir', {bdir}, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', numCon);
            break;
            %----------------------------------------------------------------------
        case 3 % deletion of single elements
            %----------------------------------------------------------------------
            % this is basically the same as in case 1, except that an element gets
            % deleted instead of copied. on the other hand it is now possible that
            % a branch gets deleted, if the removed element is exactly the starting
            % point of a branch.
            if size(bdir{bNumber},1) < 2 % prevent empty dir files
                continue;
            end
            
            delElem = bdir{bNumber}(mSpot,:); % direction vector of the deleted element
            
            tempVec = toChange; % toChange is needed later on, temporary store
            while any(toChange) % adjustment of the start vectors
                for child=1:size(toChange,1)
                    startVec(toChange(child),:) = startVec(toChange(child),:) - delElem;
                end
                toChange = find(ismember(parentVec, toChange));
            end
            toChange = tempVec;
            % the branches connecting to the removed elements have to be deleted.
            % additionally the indices in the parent vector have to be adjusted
            toDelete = false(size(parentVec));  % stores true at the number of each
            % branch that is getting deleted
            if any(toChange)
                elemLoc = startVec(bNumber,:) + sum(bdir{bNumber}(1:mSpot,:),1); %location of the deleted element
                for a = 1:size(toChange,1)
                    if startVec(toChange(a),:) == elemLoc
                        toDelete(toChange(a),1) = true;
                    end
                end
            end
            toDelete = find(toDelete);  % erase all zero elements and get the
            % correct indices
            % if a branch gets removed and this branch has sidebranches, those will
            % have to be removed as well, this would otherwise result in a
            % non-physical structure.
            deleteVec = false(size(parentVec));
            
            while any(toDelete)
                deleteVec(toDelete,1) = true;
                toDelete = find(ismember(parentVec, toDelete)); % children of the branches that are getting deleted
            end
            deleteVec = find(deleteVec); % remove zeros and get correct indices
            
            %the indices in the parent vector have to get modified. this is because
            %of the shift in indices due to branches getting removed.
            if any(deleteVec)
                
                newIndex = parentVec;
                for i=1:size(deleteVec,1)
                    
                    % if the deleted branch is the last branch in the parent vector it
                    % will - once deleted - for the same reason not influence the other
                    % indices
                    if deleteVec(i) == size(parentVec,1)
                        continue;
                    end
                    
                    % parent stores all the branch numbers whose indices get changed.
                    % these indices are following the one index that is getting
                    % deleted. so each one of these will get shifted one number to the
                    % front.
                    parent = zeros(size(parentVec,1) - deleteVec(i),1);
                    for k=deleteVec(i)+1:size(parentVec,1)
                        parent(k-deleteVec(i)) = k;
                    end
                    
                    % child stores every element that has a parent, whose index changed
                    child = find(ismember(parentVec,parent));
                    
                    if any(child) % shifting the index
                        for j=1:size(child,1)
                            newIndex(child(j)) = newIndex(child(j)) - 1;
                        end
                    end
                    
                end
                
                parentVec = newIndex; % needs to be done before the deletion
                % the actual deletion:
                parentVec(deleteVec,:) = [];
                startVec(deleteVec,:) = [];
                bdir(deleteVec) = [];
            end
            
            bdir{bNumber}(mSpot,:) = [];
            
            if useStability
                [stable,maxStress,maxDisplacement,numCon] = checkstability(bdir,startVec,externalLoad,externalLoadPosition,useObstacles,obstacles);
                if stable ~= 1
                    % disp(stable);  % for debugging
                    continue;
                end
            else
                if checkcollision(bdir,startVec) == false
                    continue;
                end
            end
            
            mutate = struct('parentVec', parentVec, 'startVec', startVec, 'bdir', {bdir}, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', numCon);
            break;
            %----------------------------------------------------------------------
        case 4 % rotation about one of the three cartesian axes
            %----------------------------------------------------------------------
            rotType = randi(6);
            rotMat = RotationMatrix((rotType-1)*3+1:(rotType)*3,:);
            
            %rotLoc is the location vector of the rotating element. be careful: it is
            %only correct, because all elements are symmetric to all implemented
            %rotations. so the cube that would actually be rotating can stay unchanged,
            %only the direction vectors of the following elements need to be rotated.
            rotLoc = startVec(bNumber,:) + sum(bdir{bNumber}(1:(mSpot-1),:),1);
            if mSpot <= 1
                bdir{bNumber} = bdir{bNumber}*rotMat;
            else
                bdir{bNumber} = [bdir{bNumber}(1:mSpot-1,:);bdir{bNumber}(mSpot:end,:)*rotMat];
            end
            
            while any(toChange)
                for child = 1:size(toChange,1)
                    startVec(toChange(child),:) = (startVec(toChange(child),:) - rotLoc)*rotMat + rotLoc;
                    bdir{toChange(child)} = bdir{toChange(child)}*rotMat;
                end
                toChange = find(ismember(parentVec, toChange));
            end
            
            if useStability
                [stable,maxStress,maxDisplacement,numCon] = checkstability(bdir,startVec,externalLoad,externalLoadPosition,useObstacles,obstacles);
                if stable ~= 1
                    % disp(stable);  % for debugging
                    continue;
                end
            else
                if checkcollision(bdir,startVec) == false
                    continue;
                end
            end
            
            mutate = struct('parentVec', parentVec, 'startVec', startVec, 'bdir', {bdir}, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', numCon);
            break;
            %----------------------------------------------------------------------
        case 5 % growing a branch
            %----------------------------------------------------------------------
            % allow the origin to be the source of a branch.
            if bNumber == 1
                mSpot = mSpot - 1;
            end
            % the added branch has a random length.
            bSizeMin = 1;
            bSizeMax = round(mSize/2);
            % UPDATE 6.7. instead of 5 changed to mSize/2
            bSize = randi([bSizeMin bSizeMax]);
            
            [branch, flag] = addBranch(bSize);
            if flag
                continue;
            else
                startVec_new = startVec(bNumber,:) + sum(bdir{bNumber}(1:mSpot,:),1);
                bdir{nBranch+1} = branch;
                parentVec(nBranch+1,1) = bNumber;
                startVec(nBranch+1,:) = startVec_new;
            end
            % UPDATE 8.7. added check for adding branch
            
            
            if useStability
                [stable,maxStress,maxDisplacement,numCon] = checkstability(bdir,startVec,externalLoad,externalLoadPosition,useObstacles,obstacles);
                if stable ~= 1
                    % disp(stable);  % for debugging
                    continue;
                end
            else
                if checkcollision(bdir,startVec) == false
                    continue;
                end
            end
            
            mutate = struct('parentVec', parentVec, 'startVec', startVec, 'bdir', {bdir}, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', numCon);
            break;
            
        otherwise
            disp('Unknown Mutation Type. Mutation aborted!');
            break;
            
    end
    
    
end

end