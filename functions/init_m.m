function initPars = init_m()
%--------------------------
% Initializes the problem: sets up the enviroment, defining the number of
% generations, number of cubes, number of organisms per generation
% Initializes the enviroment: sets up the obstacles, the target(s) and
% loads
%--------------------------

clear variables;
close all;
clc

addpath functions
addpath Truss_Analysis
addpath movieFrames

%Starting parameters:
initPars.maxGen = 30;                                   % maximum number of generations
initPars.nCubeMin = 25;                                  % minimum number of starting cubes
initPars.nCubeMax = 100;                                  % maximum number of starting cubes
initPars.nPopulation = 10;                               % size of the population
initPars.nElite = 1;                                     % size of the elite
initPars.mutationPercent = 0.6;                          % percentage of mutations wrt cross overs
initPars.maxOps=50;

initPars.nMutate=round(initPars.mutationPercent*(initPars.nPopulation-initPars.nElite)); %number of mutations
initPars.nCrossover = initPars.nPopulation - initPars.nElite - initPars.nMutate;    % number of cross overs

%Defines the target, loads
initPars.destination=[0 10 8];      %2d for now % coordinates of the destination point
initPars.externalLoad = [0 5 10];                         % summation of all external forces
initPars.externalLoadPosition = initPars.destination;     % position of applied force

%Defines the obstacles
% obstacles = [1 0 0; -1 0 0; 0 1 0; 0 -1 0; 1 1 0; -1 -1 0; 1 -1 0; -1 1 0;
%             1 0 1; -1 0 1; 0 1 1; 0 -1 1; 1 1 1; -1 -1 1; 1 -1 1; -1 1 1;
%             1 0 2; -1 0 2; 0 1 2; 0 -1 2; 1 1 2; -1 -1 2; 1 -1 2; -1 1 2;
%             1 0 -1; -1 0 -1; 0 1 -1; 0 -1 -1; 1 1 -1; -1 -1 -1; 1 -1 -1; 
%             -1 1 -1; 0 0 -1]; % around base

% obstacles =    [0 0 -1; 2 -2 -1; 2 -2 0; 2 -2 1; 2 -2 2; 2 -2 3;  
%                 2 -1 -1; 2 -1 0; 2 -1 1; 2 -1 2; 2 -1 3; 
%                 1 -2 -1; 1 -2 0; 1 -2 1; 1 -2 2; 1 -2 3;
%                 2 0 -1; 2 0 0; 2 0 1; 2 0 2; 2 0 3; 
%                 0 -2 -1; 0 -2 0; 0 -2 1; 0 -2 2; 0 -2 3;]; % wall

initPars.obstacles=[0 0 -10];

end