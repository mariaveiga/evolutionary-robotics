function mutatedgene = mutation_m(gene1)
%Receives a gene and figures out the number of mutations and which
%mutations to do
%---------
%--Method:
%---------
%1. Each instruction has a probability x to mutate
%2. 
%3.



n1 = size(gene1,1);

%randompt = randi(n1);
%mutatedgene=gene1;
maxIt=100;
good=0;
w=0;
while(good~=1 || w <= maxIt)
mutatedgene=gene1; %go back to original mutation
j=1;
for i = 1:n1
    p=rand(1);
    if p<=0.2 %probability of mutation
        mutatedindex(j)=i;
        j=j+1;
    end
end

%we have the indices of mutation, for each index, we decide to either
%switch to another instruction, remove it or duplicate it, all with equal
%probability

if (exist('mutatedindex') == 0)
    mutatedindex=randi(n1);
end

for i=mutatedindex 
    mutation=randi(3);
    switch mutation
        case{1}    %remove an arbitrary instruction
        mutatedgene(i)=0;

        case{2}    %repeat an arbitrary instruction
%         mutatedgene=zeros(n1+1,1);
%         mutatedgene(1:i)=gene1(1:i);
%         mutatedgene(i+1)=gene1(i);
%         mutatedgene((i+1):n1)=gene1((i+1):n1);
%         i
%         n1
%         k=mutatedgene(1:i)
%         q=mutatedgene(i)
%         l=mutatedgene(i+1:end)
        mutatedgene1=[mutatedgene(1:i); mutatedgene(i); mutatedgene(i+1:end)];
        mutatedgene=mutatedgene1;
        
        case{3}    %transition to another guy
        %probability to switch to another guy (or remain the same)
        swap=randi(6);
        mutatedgene(i)=swap;
    end
end
        mutatedgene(mutatedgene==0) = []; %collapse the entry
        
        good=genecheck(mutatedgene);
        w=w+1;
end

end
