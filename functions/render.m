function render(organism)
    
    figure
    axis xy
    axis off
    axis equal
    view(3);
    startVec = organism.startVec;
    bdir = organism.bdir;
    %the first cube is located at the origin and black
    % -> create_cube(location,color)
    create_cube(startVec(1,:),[0.1 0.1 0.1]);
    for i=1:size(bdir,2) %for every branch
        coordVec = startVec(i,:);
        for j=1:size(bdir{i},1) %for every element of a specific branch
            coordVec = coordVec + bdir{i}(j,:);
            create_cube(coordVec,[0.7 0.7 0.7]);
        end
    end
end