function [allright,maxStress,maxDisplacement,num_con] = checkstability(bdir,startVec,externalLoad,externalLoadPosition,useObstacles,obstacles)
% allright = 1 - good
% allright = 2 - stress in structure too high
% allright = 3 - stress between structure and ground too high
% allright = 4 - colision
% allright = 5 - obstacle

%% Initialize
allright = 1;
maxStress = 0;
maxDisplacement = 0;
num_con = 0;

%% Parameters
nInternalConnections = 28;  % Number of connections within wood cube
nInterCubeConnections = 16; % Number of connections between cubes
nNodesPerCube = 8;          % Number of Nodes for each cube

aCube = 0.02;               % sidelength of a cube in meter
tHMA = 0.002;%0.001;           % width of the glue layer in meter
tMargin = 0;%0.001;            % distance between outer surface and node

rho_cube = 700;            % density wood [kg/m^3]
rho_HMA = .970;             % density glue [kg/m^3]

E_HMA = 8.9e6;              % Youngs Modulus of the glue [Pa]
E_wood = 9e9;               % Youngs Modulus of the cube material [Pa]
% UPDATE 8.7. changed values for Young's Modulus from 8.9e8to 8.9e6 and
% from 1e9 to 9e9


%% Main Program
%------------------------------basic idea---------------------------------%
%the idea is that the geometric structure gets converted into a structure
%work. a function for calculating the forces already exists, so my data
%structure has to be converted into an applicable input to the function. as
%a result the structure isn't continuous, instead it is modeled only with
%hinged colums.


%------------------initialize and prepare the location Matrix ------------%
%Step 1: create a coordinate vector, which stores the location of each cube
%in cartesian coordinates. at the same time a collision check can be
%performed. if two elements overlap, the function returns false.
%Step 2: create a 3D location matrix. this matrix stores the relative
%positions of each element. relative, because the indices don't represent
%the absolute location (the matrix should have minimal size, due to memory
%efficiency), they just describe if a cube has neighbors and so the
%corresponding connections can be made. this is neccessary to create the
%right input for the stability check function.

nElements = 1; % account for the cube at the origin

for i = 1:size(startVec,1) % sum over all elements of each branch
    nElements = nElements + size(bdir{i},1);
end

coordVec = zeros(nElements,3);
coordVec(1,:) = [0 0 0]; % again the startcube at the origin
index = 2;
nodeForce = NaN;

for j = 1:size(startVec,1)
    coordinates = startVec(j,:);
    for k = 1:size(bdir{j},1)
        coordinates = coordinates + bdir{j}(k,:);
        coordVec(index,:) = coordinates;
        
        % collision check: comparison with every foregoing element
        for c = 1:index-1
            if coordinates == coordVec(c,:) % collision happens
                allright = 4;
                return;
            end
        end
        
        % find nodes where force is applied
        if isequal(coordinates,externalLoadPosition)
            nodeForce = index;
        end
        
        % check for collision with obstacles
        if useObstacles
            for e = 1:size(obstacles,1)
                if isequal(coordinates,obstacles(e,:))
                    allright = 5;
                    return;
                end
            end
        end
        
        index = index + 1;
    end
end

sizeX = max(coordVec(:,1)) - min(coordVec(:,1));
sizeY = max(coordVec(:,2)) - min(coordVec(:,2));
sizeZ = max(coordVec(:,3)) - min(coordVec(:,3));
locMat = zeros(sizeX+1,sizeY+1,sizeZ+1); % this guarantees a minimal-sized matrix
%UPDATE 29.6. added +1 to each size length

%convert the coordinates into the indices of the 3D matrix
%note: these incides are not the real coordinates, they are modified such
%that the structure fits into the minimal-sized matrix.
shift = [1-min(coordVec(:,1)) 1-min(coordVec(:,2)) 1-min(coordVec(:,3))];
coordFit(:,1) = coordVec(:,1) + shift(1);
coordFit(:,2) = coordVec(:,2) + shift(2);
coordFit(:,3) = coordVec(:,3) + shift(3);

for l = 1:nElements
    locMat(coordFit(l,1),coordFit(l,2),coordFit(l,3)) = l;
end

%---------------------create nodes for every element ---------------------%
%Step 3:define the exact location of each node. each element consists of
%eight nodes placed near or exactly on each corner(depends on tMargin).
%Step 4:connect the nodes, such that normal-, shear- and torque-stress can
%be modeled. the nodes are connected like the edges of a cube, additionally
%fortified with all four space diagonals and the two side diagonals on each
%side. so in total there are 28 connections in each cube and 16 between
%connecting cubes.

nodeMatrix = zeros(8*nElements,3);
linkMatrix = zeros(2,nInternalConnections*nElements);
indexN = 0;                 % index for the nodes
indexL = 0;                 % index for the links
aElement = aCube + tHMA;    % sidelength of a whole building block (+glue)
aNode = aCube - 2*tMargin;  % distance between two nodes

for m = 1:nElements
    
    % compute the nodal coordinates
    nodeMatrix(indexN+1,:) = coordVec(m,:) .* aElement + [tMargin,tMargin,tMargin];
    nodeMatrix(indexN+2,:) = nodeMatrix(indexN+1,:) + [aNode,0,0];
    nodeMatrix(indexN+3,:) = nodeMatrix(indexN+1,:) + [aNode,aNode,0];
    nodeMatrix(indexN+4,:) = nodeMatrix(indexN+1,:) + [0,aNode,0];
    nodeMatrix(indexN+5,:) = nodeMatrix(indexN+1,:) + [0,0,aNode];
    nodeMatrix(indexN+6,:) = nodeMatrix(indexN+1,:) + [aNode,0,aNode];
    nodeMatrix(indexN+7,:) = nodeMatrix(indexN+1,:) + [aNode,aNode,aNode];
    nodeMatrix(indexN+8,:) = nodeMatrix(indexN+1,:) + [0,aNode,aNode];
    
    % each node has a distinct number. the connections can be made by just
    % linking two specific numbers.
    
    % x (cube edges)
    linkMatrix(:,indexL+1) = [indexN+1;indexN+2];
    linkMatrix(:,indexL+2) = [indexN+4;indexN+3];
    linkMatrix(:,indexL+3) = [indexN+5;indexN+6];
    linkMatrix(:,indexL+4) = [indexN+8;indexN+7];
    % y (cube edges)
    linkMatrix(:,indexL+5) = [indexN+1;indexN+4];
    linkMatrix(:,indexL+6) = [indexN+2;indexN+3];
    linkMatrix(:,indexL+7) = [indexN+5;indexN+8];
    linkMatrix(:,indexL+8) = [indexN+6;indexN+7];
    % z (cube edges)
    linkMatrix(:,indexL+9) = [indexN+1;indexN+5];
    linkMatrix(:,indexL+10) = [indexN+2;indexN+6];
    linkMatrix(:,indexL+11) = [indexN+3;indexN+7];
    linkMatrix(:,indexL+12) = [indexN+4;indexN+8];
    % zx (side diagonals)
    linkMatrix(:,indexL+13) = [indexN+1;indexN+6];
    linkMatrix(:,indexL+14) = [indexN+5;indexN+2];
    linkMatrix(:,indexL+15) = [indexN+4;indexN+7];
    linkMatrix(:,indexL+16) = [indexN+8;indexN+3];
    % yz (side diagonals)
    linkMatrix(:,indexL+17) = [indexN+2;indexN+7];
    linkMatrix(:,indexL+18) = [indexN+6;indexN+3];
    linkMatrix(:,indexL+19) = [indexN+1;indexN+8];
    linkMatrix(:,indexL+20) = [indexN+5;indexN+4];
    % xy (side diagonals)
    linkMatrix(:,indexL+21) = [indexN+1;indexN+3];
    linkMatrix(:,indexL+22) = [indexN+2;indexN+4];
    linkMatrix(:,indexL+23) = [indexN+5;indexN+7];
    linkMatrix(:,indexL+24) = [indexN+6;indexN+8];
    % xyz (space diagonals)
    linkMatrix(:,indexL+25) = [indexN+1;indexN+7];
    linkMatrix(:,indexL+26) = [indexN+2;indexN+8];
    linkMatrix(:,indexL+27) = [indexN+4;indexN+6];
    linkMatrix(:,indexL+28) = [indexN+3;indexN+5];
    
    indexN = indexN + nNodesPerCube;
    indexL = indexL + nInternalConnections;
    
end

%---------------create connections between the elements-------------------%
%Step 5:depending on the location, the cubes are connected by HMA. to
%account for this connection, additional pendulum supports have to be
%integrated to link the elements. to avoid double linking, neighboring
%elements are only checked in one direction (in this case in -x, -y and -z
%direction. note: it actually doesn't matter which connection is for which
%element, one fail is enough to prevent a successful stability check and it
%doesn't matter where it fails

nConnection = zeros(nElements,1); %this vector keeps track of the number of
%connections an element has. for every connection half of the HMA weight of
%a glued joint is added to the total element weight. the sum of the stored
%values is always double the actual number of connections..

conMat = zeros(6*nElements,3); % in the first and second row, the matrix
%stores the two elements connecting and in the third, the direction of the
%connection(x = 1,y = 2 or z = 3). this is relevant for the stress
%calculation, where the direction of the forces needs to be known.

for n = 1:nElements
    
    % elem A denotes the element currently beeing checked
    % elem B denotes an element that is connected to the checked one
    
    eC = coordFit(n,:); %short for element coordinates (indices of A in the
    %location matrix)
    indexA = nNodesPerCube*(n-1); %the first minus one node index of element A
    
    if eC(1) > 1 && locMat(eC(1)-1,eC(2),eC(3)) ~= 0 % checking negative X
        
        elemB = locMat(eC(1)-1,eC(2),eC(3));
        indexB = nNodesPerCube*(elemB-1);
        nConnection(n,1) = nConnection(n,1) + 1;
        nConnection(elemB,1) = nConnection(elemB,1) + 1;
        conMat(sum(nConnection)/2,:) = [n,elemB,1]; % elemA = n
        % x
        linkMatrix(:,indexL+1) = [indexA+1;indexB+2];
        linkMatrix(:,indexL+2) = [indexA+5;indexB+6];
        linkMatrix(:,indexL+3) = [indexA+8;indexB+7];
        linkMatrix(:,indexL+4) = [indexA+4;indexB+3];
        % xy
        linkMatrix(:,indexL+9) = [indexA+1;indexB+3];
        linkMatrix(:,indexL+10) = [indexA+5;indexB+7];
        linkMatrix(:,indexL+11) = [indexA+8;indexB+6];
        linkMatrix(:,indexL+12) = [indexA+4;indexB+2];
        % zx
        linkMatrix(:,indexL+5) = [indexA+1;indexB+6];
        linkMatrix(:,indexL+6) = [indexA+5;indexB+2];
        linkMatrix(:,indexL+7) = [indexA+8;indexB+3];
        linkMatrix(:,indexL+8) = [indexA+4;indexB+7];
        % xyz
        linkMatrix(:,indexL+13) = [indexA+1;indexB+7];
        linkMatrix(:,indexL+14) = [indexA+5;indexB+3];
        linkMatrix(:,indexL+15) = [indexA+8;indexB+2];
        linkMatrix(:,indexL+16) = [indexA+4;indexB+6];
        
        indexL = indexL + nInterCubeConnections;
        
    end
    
    if eC(2) > 1 && locMat(eC(1),eC(2)-1,eC(3)) ~= 0 % checking negative Y
        
        elemB = locMat(eC(1),eC(2)-1,eC(3));
        indexB = 8*(elemB-1);
        nConnection(n,1) = nConnection(n,1) + 1;
        nConnection(elemB,1) = nConnection(elemB,1) + 1;
        conMat(sum(nConnection)/2,:) = [n,elemB,2];
        % y
        linkMatrix(:,indexL+1) = [indexA+2;indexB+3];
        linkMatrix(:,indexL+2) = [indexA+6;indexB+7];
        linkMatrix(:,indexL+3) = [indexA+5;indexB+8];
        linkMatrix(:,indexL+4) = [indexA+1;indexB+4];
        % yz
        linkMatrix(:,indexL+5) = [indexA+2;indexB+7];
        linkMatrix(:,indexL+6) = [indexA+6;indexB+3];
        linkMatrix(:,indexL+7) = [indexA+5;indexB+4];
        linkMatrix(:,indexL+8) = [indexA+1;indexB+8];
        % xy
        linkMatrix(:,indexL+9) = [indexA+2;indexB+4];
        linkMatrix(:,indexL+10) = [indexA+6;indexB+8];
        linkMatrix(:,indexL+11) = [indexA+5;indexB+7];
        linkMatrix(:,indexL+12) = [indexA+1;indexB+3];
        % xyz
        linkMatrix(:,indexL+13) = [indexA+2;indexB+8];
        linkMatrix(:,indexL+14) = [indexA+6;indexB+4];
        linkMatrix(:,indexL+15) = [indexA+5;indexB+3];
        linkMatrix(:,indexL+16) = [indexA+1;indexB+7];
        
        indexL = indexL + nInterCubeConnections;
        
    end
    
    if eC(3) > 1 && locMat(eC(1),eC(2),eC(3)-1) ~= 0 % checking negative Z
        
        elemB = locMat(eC(1),eC(2),eC(3)-1);
        indexB = 8*(elemB-1);
        nConnection(n,1) = nConnection(n,1) + 1;
        nConnection(elemB,1) = nConnection(elemB,1) + 1;
        conMat(sum(nConnection)/2,:) = [n,elemB,3];
        % z
        linkMatrix(:,indexL+1) = [indexA+3;indexB+7];
        linkMatrix(:,indexL+2) = [indexA+2;indexB+6];
        linkMatrix(:,indexL+3) = [indexA+1;indexB+5];
        linkMatrix(:,indexL+4) = [indexA+4;indexB+8];
        % zx
        linkMatrix(:,indexL+5) = [indexA+3;indexB+8];
        linkMatrix(:,indexL+6) = [indexA+2;indexB+5];
        linkMatrix(:,indexL+7) = [indexA+1;indexB+6];
        linkMatrix(:,indexL+8) = [indexA+4;indexB+7];
        % yz
        linkMatrix(:,indexL+9) = [indexA+3;indexB+6];
        linkMatrix(:,indexL+10) = [indexA+2;indexB+7];
        linkMatrix(:,indexL+11) = [indexA+1;indexB+8];
        linkMatrix(:,indexL+12) = [indexA+4;indexB+5];
        % xyz
        linkMatrix(:,indexL+13) = [indexA+3;indexB+5];
        linkMatrix(:,indexL+14) = [indexA+2;indexB+8];
        linkMatrix(:,indexL+15) = [indexA+1;indexB+7];
        linkMatrix(:,indexL+16) = [indexA+4;indexB+6];
        
        indexL = indexL + nInterCubeConnections;
        
    end
    
end

conMat = conMat(1:sum(nConnection)/2,:); % cut off the unnecessary zeros
num_con = size(conMat,1);
%-----------------------create the force/load vector----------------------%
%Step 6: right now, only the gravity force is getting implemented. later on
%additional weight or load can be added, according to the function the
%structure should execute. the gravity is evenly distributed between the
%nodes.


mCube = rho_cube * aCube^3;          % mass of cube
mHMA = rho_HMA * tHMA * aCube^2 / 2; % mass of HMA
% the HMA mass is halved, because two elements share a glued joint
g = 9.81; % [m/s^2]

load = zeros(nNodesPerCube*nElements,3);
% UPDATE 7.7. nNodesPerCube*nELements instead of nElements

iter = 0;

f_G = (-(mCube+nConnection(l,1)*mHMA)*g)/nNodesPerCube; % calculate the gravitational force
for l = 1:nElements
    if l == nodeForce
        for k = 1:nNodesPerCube
            load(iter+k,:) = [0 0 f_G] + externalLoad./nNodesPerCube; % add the external force
        end
    else
        for k = 1:nNodesPerCube
            load(iter+k,:) = [0 0 f_G]; % add the load in z-direction
        end
    end
    iter = iter + nNodesPerCube;
end

%-------------------DOF, Modulus of Elasticity and Area-------------------%
%Step 7: The last three inputs for the algorithm get calculated. First the
%degrees of freedom, where one can define the static nodes. Second the
%Youngs Modulus for each pendulum support and third the area of each
%support. The sidearea of a cube gets divided by the number of pendulum
%supports in an individual cubes.

% defines the DOF for each node in each direction, 1->fixed, 0->free
DOF = zeros(indexN,3);
DOF([1,2,3,4],:) = ones(4,3);
%only the original cube is directly connected to the robot. the connecting
%area is defined here, as the x,y-plane side of the cube

E = ones(1,indexL).*E_HMA;
E(1,1:nInternalConnections*nElements) = E_wood;

areaHMA = aCube^2 / nInterCubeConnections; % area of external pendulum supports
areaWood = aCube^2 / nInternalConnections; % area of internal pendulum supports
A = ones(1,indexL).*areaHMA;
A(1,1:nInternalConnections*nElements) = areaWood;

%-----------------Convert and compute the truss stability-----------------%
%Step 8:Combine the data into a struct as input to the algorithm and
%compute the forces and deflections.
%Step 9:Compute the normal- , shear-, torquestress and the momentum in each
%glued joint and compare it with the maximum allowed values to detect
%failures in the structure.

D = struct('Coord',nodeMatrix','Con',linkMatrix,'Re',DOF','Load',load','E',E','A',A');

% F are the forces acting in the pendulum supports
% U are the deflections of the nodes
% R are the reaction forces on the fixed nodes
[F,U,R] = ST(D);
% TP(D,U,0);
% disp(R(:,1:4)); % for debugging
% disp(F(:,9:12)); % for debugging
% normal-, shear- and torquestress calculation (all stresses in N/m^2)
criticalStress = 1.2e5; % [N/m^2]

displacement = zeros(size(U,2),1);
for i=1:size(U,2)
    displacement(i) = norm(U(:,i)');
end
maxDisplacement=max(displacement);
% UPDATE 11.7. added displacement

indexS = nInternalConnections*nElements;
for link = 1:size(conMat,1)
    
    indexA = 8*(conMat(link,1)-1);
    indexB = 8*(conMat(link,2)-1);
    % nodeVec stores the coordinates of the eight corner nodes of the
    % connection. the order depends on the direction of the connection.
    switch conMat(link,3)
        case 1 % connection in x direction
            nodeVec = [nodeMatrix(indexA+1,:) + U(:,indexA+1)';
                nodeMatrix(indexA+5,:) + U(:,indexA+5)';
                nodeMatrix(indexA+8,:) + U(:,indexA+8)';
                nodeMatrix(indexA+4,:) + U(:,indexA+4)';
                nodeMatrix(indexB+2,:) + U(:,indexB+2)';
                nodeMatrix(indexB+6,:) + U(:,indexB+6)';
                nodeMatrix(indexB+7,:) + U(:,indexB+7)';
                nodeMatrix(indexB+3,:) + U(:,indexB+3)'];
        case 2 % connection in y direction
            nodeVec = [nodeMatrix(indexA+2,:) + U(:,indexA+2)';
                nodeMatrix(indexA+6,:) + U(:,indexA+6)';
                nodeMatrix(indexA+5,:) + U(:,indexA+5)';
                nodeMatrix(indexA+1,:) + U(:,indexA+1)';
                nodeMatrix(indexB+3,:) + U(:,indexB+3)';
                nodeMatrix(indexB+7,:) + U(:,indexB+7)';
                nodeMatrix(indexB+8,:) + U(:,indexB+8)';
                nodeMatrix(indexB+4,:) + U(:,indexB+4)'];
        case 3 % connection in z direction
            nodeVec = [nodeMatrix(indexA+3,:) + U(:,indexA+3)';
                nodeMatrix(indexA+2,:) + U(:,indexA+2)';
                nodeMatrix(indexA+1,:) + U(:,indexA+1)';
                nodeMatrix(indexA+4,:) + U(:,indexA+4)';
                nodeMatrix(indexB+7,:) + U(:,indexB+7)';
                nodeMatrix(indexB+6,:) + U(:,indexB+6)';
                nodeMatrix(indexB+5,:) + U(:,indexB+5)';
                nodeMatrix(indexB+8,:) + U(:,indexB+8)'];
    end
    % compute the normal vector of the plane through the four nodes of
    % element A.
    nVec = cross(nodeVec(3,:)-nodeVec(1,:),nodeVec(4,:)-nodeVec(2,:));
    [~, index] = max(abs(nVec));
    normVec = zeros(1,3);
    normVec(1,index) = nVec(1,index);
    % UPDATE 7.7. added correction for numerical errors (normal will always
    % point in direction of axis)
    normVec = normVec ./ norm(normVec);
    midSideA = sum(nodeVec(1:4,:),1) ./ 4; % middle of the four A nodes
    midSideB = sum(nodeVec(5:8,:),1) ./ 4; % middle of the four B nodes
    center = (midSideA + midSideB) ./ 2;   % center of the whole glued joint
    
    % compute the unit vector of each pendulum support pointing from A to B
    
    unitVec = zeros(nInterCubeConnections,3);
    % either x,y or z direction
    unitVec(1,:) = nodeVec(5,:) - nodeVec(1,:);
    unitVec(2,:) = nodeVec(6,:) - nodeVec(2,:);
    unitVec(3,:) = nodeVec(7,:) - nodeVec(3,:);
    unitVec(4,:) = nodeVec(8,:) - nodeVec(4,:);
    % side diagonals
    unitVec(5,:) = nodeVec(8,:) - nodeVec(1,:);
    unitVec(6,:) = nodeVec(7,:) - nodeVec(2,:);
    unitVec(7,:) = nodeVec(6,:) - nodeVec(3,:);
    unitVec(8,:) = nodeVec(5,:) - nodeVec(4,:);
    unitVec(9,:) = nodeVec(6,:) - nodeVec(1,:);
    unitVec(10,:) = nodeVec(5,:) - nodeVec(2,:);
    unitVec(11,:) = nodeVec(8,:) - nodeVec(3,:);
    unitVec(12,:) = nodeVec(7,:) - nodeVec(4,:);
    % space diagonals
    unitVec(13,:) = nodeVec(7,:) - nodeVec(1,:);
    unitVec(14,:) = nodeVec(8,:) - nodeVec(2,:);
    unitVec(15,:) = nodeVec(5,:) - nodeVec(3,:);
    unitVec(16,:) = nodeVec(6,:) - nodeVec(4,:);
    
    for uV = 1:nInterCubeConnections % normalize the length
        unitVec(uV,:) = unitVec(uV,:) ./ norm(unitVec(uV,:));
    end
    
    
    % multiply the unit vector with the absolute value of the force in the
    % corresponding pendulum support.
    totalForce = zeros(nInterCubeConnections,3); % stores the forces in
    % each node
    for uV = 1:nInterCubeConnections
        totalForce(uV,:) = unitVec(uV,:) .* F(indexS+uV);
    end
    totalForce = sum(totalForce,1);
    
    normalForce = dot(totalForce,normVec)*normVec;
    shearForce = totalForce - normalForce;
    % UPDATE 7.7. changed the way normal and shear force are computed
    % (remove norm)...
    % For example: normVec = [-1 0 0], totalForce = [0.8 0 -0.3]
    % then normalForce = norm([0.8 0 -0.3]*[-1 0 0]) = 0.8 and
    % shearForce = norm([0.8 0 -0.3]-0.8*[-1 0 0]) = norm([1.6 0 -0.3])
    % This is wrong! Also if we take the norm we don't know the direction
    % (w.r.t. normVec) of the force!
    
    % the resulting strain of bending and torque gets approximated by
    % simply adding them to normal respectively shear stress after
    % multiplying with a quarter of the cube sidelength. this approximation
    % is poposed by luzius. also compressive stress is neglected, because
    % the glue is assumed to fail only under traction force.
    
    totalMomentum = zeros(nInterCubeConnections,3); % keeps track of all
    % induced momenta in the center of the glued joint
    
    midNode = zeros(nInterCubeConnections,3); % mid node coordinates
    
    % either x,y or z direction
    midNode(1,:) = (nodeVec(5,:) + nodeVec(1,:))/2;
    midNode(2,:) = (nodeVec(6,:) + nodeVec(2,:))/2;
    midNode(3,:) = (nodeVec(7,:) + nodeVec(3,:))/2;
    midNode(4,:) = (nodeVec(8,:) + nodeVec(4,:))/2;
    % side diagonals
    midNode(5,:) = (nodeVec(8,:) + nodeVec(1,:))/2;
    midNode(6,:) = (nodeVec(7,:) + nodeVec(2,:))/2;
    midNode(7,:) = (nodeVec(6,:) + nodeVec(3,:))/2;
    midNode(8,:) = (nodeVec(5,:) + nodeVec(4,:))/2;
    midNode(9,:) = (nodeVec(6,:) + nodeVec(1,:))/2;
    midNode(10,:) = (nodeVec(5,:) + nodeVec(2,:))/2;
    midNode(11,:) = (nodeVec(8,:) + nodeVec(3,:))/2;
    midNode(12,:) = (nodeVec(7,:) + nodeVec(4,:))/2;
    % space diagonals
    midNode(13,:) = (nodeVec(7,:) + nodeVec(1,:))/2;
    midNode(14,:) = (nodeVec(8,:) + nodeVec(2,:))/2;
    midNode(15,:) = (nodeVec(5,:) + nodeVec(3,:))/2;
    midNode(16,:) = (nodeVec(6,:) + nodeVec(4,:))/2;
    
    for e = 1:nInterCubeConnections
        totalMomentum(e,:) = cross(unitVec(e,:).*F(indexS+e),center-midNode(e,:));
    end
    totalForceByMomentum = sum(totalMomentum,1);
    
    torqueMomentum = dot(totalForceByMomentum,normVec)*normVec;
    bendingMomentum = totalForceByMomentum - torqueMomentum;
    % UPDATE 7.7. correction same as for normal and shear force
    
    if (normalForce*normVec'<0)
        normalForce = [0 0 0];
    end
    % UPDATE 7.7. if normalForce and normVec have different signs that
    % means that we have compression so we don't need to take it into
    % account
    
    normalStress = (norm(normalForce + bendingMomentum / (aCube/2)))/aCube^2;
    shearStress = (norm(shearForce + torqueMomentum / (aCube/2)))/aCube^2;
    % UPDATE 7.7. divide instead of multiply
    % UPDATE 7.7. add norm to normal and shear stress
    
    comparisonStress = sqrt(normalStress^2 + 3*shearStress^2);
    if comparisonStress > maxStress % the max stress should be an output to further compare the organisms
        maxStress = comparisonStress;
    end
    
    if comparisonStress > criticalStress % connection fails
        allright = 2;
        return;
    end
    
    indexS = indexS + nInterCubeConnections; % adapt the index to the next element
    
end

% finally we have to check the reaction forces on the fixed nodes
reaction = R(:,1:4)';
reaction = sum(reaction,1);
normVec = [0 0 -1];
normalForce = dot(reaction,normVec)*normVec;
shearForce = reaction - normalForce;
% UPDATE 7.7. changed names

center = sum(nodeMatrix(1:4,:),1)/4;
totalMomentum = zeros(4,3);
for i = 1:4
    totalMomentum(i,:) = cross(R(:,i),center-nodeMatrix(i,:));
end
totalForceByMomentum = sum(totalMomentum,1);

torqueMomentum = dot(totalForceByMomentum,normVec) * normVec;
bendingMomentum = totalForceByMomentum - torqueMomentum;
% UPDATE 7.7. changed as above

if (normalForce*normVec'<0)
    normalForce = [0 0 0];
end
% UPDATE 7.7. if normalForce and normVec have different signs that
% means that we have compression so we don't need to take it into
% account

normalStress = (norm(normalForce + bendingMomentum / (aCube/2)))/aCube^2;
shearStress = (norm(shearForce + torqueMomentum / (aCube/2)))/aCube^2;
% UPDATE 7.7. divide instead of multiply and add norm

comparisonStress = sqrt(normalStress^2 + 3*shearStress^2);
if comparisonStress > maxStress
    maxStress = comparisonStress;
end

if comparisonStress > criticalStress % connection fails
    allright = 3;
    return;
end

end