function [fitness] = fitnessfunction(gene,organism,destination,useDeflection,useStress,useCubes,useConnections)

%startVec = organism.startVec;
%parentVec = organism.parentVec;
%bdir = organism.bdir;
maxStress = organism.maxStress;
maxDisplacement = organism.maxDisplacement;
numCon = organism.numCon;
% UPDATE 11.7. added displacement

%nBranch = size(parentVec,1);

nTarget = size(destination,1); % amount of destination points

nTot = sum(cellfun('size',bdir,1)) + 1; % total amount of elements. the
                                        % plus one accounts for  the
                                        % element at the origin
                                        
% locVec = zeros(nTot,3);         % location vector of each element
% locVec(1,:) = startVec(1,:);    % element at the origin
% index = 2;
% for branch = 1:nBranch
%     coordVec = startVec(branch,:);
%     for element = 1:size(bdir{branch},1)
%         coordVec = coordVec + bdir{branch}(element,:);
%         locVec(index,:) = coordVec;
%         index = index + 1;
%     end
% end
% 


n=length(gene);
nElements=0;
for i=1:n
   if gene(i)==1
   nElements=nElements+1;
   end
end

coordVec = zeros(nElements,3);
m=size(matloc,1);
l=1; %no 3D
k=1;
for i=1:m
   for j=1:m
%       for l=1:m
       if matloc(i,j,l)~= 0
          coordVec(k,:)= [ l j m-i];
          k=k+1;
       end
%   coordVec(j,:)=matLoc(
%       end
   end
end


distVec = zeros(nTot,nTarget);  % distance between a specific element and a
                                % specific target point
for target = 1:nTarget
    for element = 1:nTot
        distVec(element,target) = norm(locVec(element,:)-destination(target,:));
    end
end

minimalDistance = zeros(1,nTarget); % the minimal distance between an
                                    % element and a specific destination
                                    % point
relativeError = zeros(1,nTarget); % the relative error for each target
for target = 1:nTarget
    [~,minIndex] = min(distVec(:,target),[],1);
    minimalDistance(1,target) = distVec(minIndex,target);
    relativeError(1,target) = minimalDistance(1,target)/norm(destination(target,:));
end

criticalStress = 1.2e5; % [N/m^2]
% UPDATE 8.7. changed critStress from 1.2e6 to 1.2e5 

stressRatio = maxStress/criticalStress; % the less stress the organism
% experiences, the higher the fitness is

distanceFactor = 1e2;
if useCubes
    weightFactor = 0.01; 
else
    weightFactor = 0;
end
if useStress
    stabilityFactor = 5; %1e-01;
else
    stabilityFactor = 0;
end
if useDeflection
    displacementFactor = 500; %1e2;
else
    displacementFactor = 0;
end
if useConnections
    connectionFactor = 0.01;
else
    connectionFactor = 0;
end
%accounts for the relative error and punishes high numbers of elements
fitness = distanceFactor*(1-sum(relativeError,2)/nTarget) - weightFactor*nTot - stabilityFactor*stressRatio - displacementFactor*maxDisplacement + connectionFactor*numCon;

end