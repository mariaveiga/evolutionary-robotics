function covergene = mix_m(gene1,gene2)
%,useStability,externalLoad,externalLoadPosition,useObstacles,obstacles)

% this function mixes two different organisms together. the mixing points
% on each organism determine the location where each gets cut. the
% remaining parts get merged together and form a new organism.

% UPDATE 6.7. add checkexisting

iter = 0;
iterCounter = 0;
iterMax = 50;
maxStress = 0; % maximal stress in the structure
maxDisplacement = 0;
numCon = 0;
% UPDATE 11.7. added displacement

%while iter < 1
    
%--------------------------------------------------------------------------
% loading and preparing the mixed structure
%--------------------------------------------------------------------------
%     startVec = organismA.startVec;
%     parentVec = organismA.parentVec;
%     bdir = organismA.bdir;
%     nBranch = size(parentVec,1);
%     mSize = sum(cellfun('size',bdir,1));
    
    good=0;
    
    while(good~=1 || iter <=iterMax)
    size1=length(gene1);
    size2=length(gene2);
    
    randompt1=randi(size1);
    randompt2=randi(size2);
    covergene=zeros(randompt1+(1+size2-randompt2),1);
    a=length(covergene);
    n3=length(gene2(randompt2:end));
    covergene(1:randompt1)=gene1(1:randompt1);
    covergene((randompt1+1):end)=gene2(randompt2:end);
    
    good=genecheck(covergene);    
    
%     
%     %mSpot = randi(mSize);
%     %tempSpot = mSpot;  % used to determine the number of elements getting added, see line 118
%     %bNumber = 1;
%     % determine the branch number on which the cut takes place
%     while mSpot > size(bdir{bNumber},1)
%         mSpot = mSpot - size(bdir{bNumber},1);
%         bNumber = bNumber + 1;
%     end
%     
%     % all branches connecting to the cut branch get stored in toRemove
%     toRemove = find(parentVec == bNumber);
%     
%     % ignore every branch connecting ahead of the cut off element
%     if any(toRemove)
%         ignSize = mSpot - 1; % the number of direction vectors up to which
%         % connecting branches can be ignored for adjusting
%         difference = false(size(toRemove));
%         % all start vectors of are getting compared to the elements ahead
%         % of the mutation spot. if they match, no modification has to be
%         % made on the corresponding branch.
%         ignStart = startVec(bNumber,:);
%         for ign=0:ignSize
%             ignoreVec = ignStart + sum(bdir{bNumber}(1:ign,:),1);
%             for k=1:size(toRemove,1)
%                 if startVec(toRemove(k,1),:) == ignoreVec
%                     difference(k,1) = true;
%                 end
%             end
%             
%         end
%         toRemove(difference,:) = []; %the ignored branches get sorted out
%     end;
%     
%     mutVec = startVec(bNumber,:) + sum(bdir{bNumber}(1:mSpot-1,:),1); % used later on, see line 173
%     
%     %sidebranches have to be removed as well
%     removeAll = false(size(parentVec));
%     while any(toRemove)
%         removeAll(toRemove,1) = true;
%         toRemove = find(ismember(parentVec, toRemove)); % find sidebranches
%     end
%     removeAll = find(removeAll);
%     
% %--------- removing the cut elements and adjusting parent vector ---------%
%     % the indices in the parent vector have to get modified. this is
%     % because of the shift in indices due to branches getting removed.
%     if any(removeAll)
%         
%         newIndex = parentVec;
%         for i=1:size(removeAll,1)
%             
%             % if the deleted branch is the last branch in the parent
%             % vector, it will - once deleted - for the same reason not
%             % influence the other indices
%             if removeAll(i) == nBranch
%                 continue;
%             end
%             
%             %parent stores all the branch numbers whose indices get changed
%             parent = zeros(size(parentVec,1)-removeAll(i),1);
%             for k=removeAll(i)+1:size(parentVec,1)
%                 parent(k-removeAll(i)) = k;
%             end
%             
%             % child stores every element that has a parent, whose index
%             % changed
%             child = find(ismember(parentVec,parent));
%             
%             if any(child) % shifting the index
%                 for j=1:size(child,1)
%                     newIndex(child(j)) = newIndex(child(j)) - 1;
%                 end
%             end
%             
%         end
%         
%         parentVec = newIndex;
%         %the actual deletion:
%         parentVec(removeAll,:) = [];
%         startVec(removeAll,:) = [];
%         bdir(:,removeAll) = [];
%     end
%     bdir{bNumber}(mSpot:end,:) = [];
%     nBranch = size(parentVec,1);
% %--------------------------------------------------------------------------
% % loading and initiating the mixing structure, denoted with *_M
% %--------------------------------------------------------------------------
%     startVec_M = organismB.startVec;
%     parentVec_M = organismB.parentVec;
%     bdir_M = organismB.bdir;
%     mSize_M = sum(cellfun('size',bdir_M,1));
%     
%     % the added part should be about the same size as the removed part.
%     nDeletedElements = mSize - (tempSpot-1); % need to change, because mSpot gets changed in the beginning!!!
%     nAddedElements = nDeletedElements + randi([-1 1]);
%     mSpot_M = mSize_M - nAddedElements;
%     
%     if mSpot_M < 1
%         mSpot_M = 1;
%     end
%     
%     bNumber_M = 1;
%     while mSpot_M > size(bdir_M{bNumber_M},1)
%         mSpot_M = mSpot_M - size(bdir_M{bNumber_M},1);
%         bNumber_M = bNumber_M + 1;
%     end
%     
% %--------------------- find out which elements to add --------------------%
% % this is exactly the same code as used above, so see above for comments.
%     
%     toAdd = find(parentVec_M == bNumber_M);
%     if any(toAdd)
%         ignSize_M = mSpot_M - 1;
%         difference_M = false(size(toAdd));
%         ignStart_M = startVec_M(bNumber_M,:);
%         
%         for ign=0:ignSize_M
%             ignoreVec_M = ignStart_M + sum(bdir_M{bNumber_M}(1:ign,:),1);
%             for k=1:size(toAdd,1)
%                 if startVec_M(toAdd(k,1),:) == ignoreVec_M
%                     difference_M(k,1) = true;
%                 end
%             end
%         end
%         toAdd(difference_M,:) = [];
%     end;
%     
%     tempAdd = toAdd;
%     nBranchAdd = 0; % total number of branches that are going to be added
%     while any(toAdd)
%         nBranchAdd = nBranchAdd + size(toAdd,1);
%         toAdd = find(ismember(parentVec_M, toAdd));
%     end
%     toAdd = tempAdd;
%     
% %---------------------- add new branches and elements --------------------%
% % adding the branches is very easy, just copy the bdir matrices. but the
% % start vectors change when transferred from one to another organism. to
% % account for this, the difference is the mSpot location is calculated
% % (mutDiff) and added to gain the proper start vectors. additionally the
% % parent vector has to get updated, because new branches mean new
% % relationships which have to be defined.
% 
%     if any(toAdd)
%         
%         % to account for the difference of the mSpots in both organisms,
%         % the difference between them is calculated
%         mutVec_M = startVec_M(bNumber_M,:) + sum(bdir_M{bNumber_M}(1:mSpot_M-1,:),1);
%         mutDiff = mutVec - mutVec_M; % difference between old and new startVec
%         
%         addCount = 1;
%         while any(toAdd)
%             
%             for i=1:size(toAdd,1) % add new branches and start vectors
%                 bdir{nBranch+addCount} = bdir_M{toAdd(i,1)};
%                 startVec(nBranch+addCount,:) = startVec_M(toAdd(i,1),:) + mutDiff;
%                 addCount = addCount + 1;
%             end
%             toAdd = find(ismember(parentVec_M,toAdd));
%         end
%         
%         index = size(parentVec,1); % amount of branches already stored
%         parentVec_new = zeros(nBranchAdd,1);
%         toAdd = tempAdd;
%         % keep track on which index number of the parent vector new
%         % branches will get added, to create the proper relationships
%         % between the old and new branches
%         index_M = size(toAdd,1); % number of branches getting added
%                                      % from the other organism
%         
%         % remember: branchAdd is storing the numbers of the branches that
%         % directly connect to the cut branch, so all these branches have
%         % the same parent, the branch with bNumber:
%         parentVec_new(1:index_M,1) = bNumber;
%         
%         alreadyAdded = toAdd; % branch numbers, which are already added
%         toAdd = find(ismember(parentVec_M,toAdd)); % get sidebranches
%         
%         while any(toAdd)
%             for m = 1:size(toAdd,1)
%                 for n = 1:size(alreadyAdded,1)
%                     if parentVec_M(toAdd(m,1)) == alreadyAdded(n,1)
%                         parentVec_new(index_M+m,1) = index + n;
%                     end
%                 end
%             end
%             index_M = index_M + size(toAdd,1); % amount of branches added
%             index = index + size(alreadyAdded,1); % total amount of branches stored
%             alreadyAdded = toAdd; % branches, which are already added
%             toAdd = find(ismember(parentVec_M,toAdd)); % once again, get sidebranches
%         end
%         parentVec(end+1:end+size(parentVec_new,1),1) = parentVec_new;
%         
%     end
%     bdir{bNumber} = [bdir{bNumber};bdir_M{bNumber_M}(mSpot_M:end,:)];

%----------------------------- check and save ----------------------------%
    
 %   else
%         if checkcollision(bdir,startVec) == false
%             iterCounter = iterCounter + 1;
%             if iterCounter > iterMax
%                 % copyfile(['../data/' name],['../data/' nameAfter]);
%                 disp('Mixing not successful. Mutating instead...');
%                 crossover = mutation(organismA,useStability,externalLoad,externalLoadPosition,useObstacles,obstacles);
%                 % UPDATE 8.7. added mutation
%                 return;
%             end
%             continue;
%         end
  %  end
    
    
    %crossover = struct('parentVec', parentVec, 'startVec', startVec, 'bdir', {bdir}, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', numCon);
    iter = iter + 1;
    
%end

end