function p = paramst() %% Initialize


%% Parameters
p.nInternalConnections = 28;  % Number of connections within wood cube
p.nInterCubeConnections = 16; % Number of connections between cubes
p.nNodesPerCube = 8;          % Number of Nodes for each cube

p.aCube = 0.02;               % sidelength of a cube in meter
p.tHMA = 0.002;%0.001;           % width of the glue layer in meter
p.tMargin = 0;%0.001;            % distance between outer surface and node

p.rho_cube = 700;            % density wood [kg/m^3]
p.rho_HMA = .970;             % density glue [kg/m^3]

p.E_HMA = 8.9e6;              % Youngs Modulus of the glue [Pa]
p.E_wood = 9e9;               % Youngs Modulus of the cube material [Pa]
% UPDATE 8.7. changed values for Young's Modulus from 8.9e8to 8.9e6 and
% from 1e9 to 9e9
end