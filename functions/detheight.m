function y = detheight(vect)
%receive a column, I want to know which which is the "height" (by checking the last non zero
%entry) of the column and retriving it's coordinate 
n = length(vect);
%vect
%y=(n-1)/2+1;
y=-1; %there are no negative indices
for i=1:n
    if vect(i)~=0
        y = i-1;
        break;
    end
end
%y;

end