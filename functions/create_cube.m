function h=create_cube(origin,color)

x=[0 1 1 0 0 0;1 1 0 0 1 1;1 1 0 0 1 1;0 1 1 0 0 0] + origin(1,1);
y=[0 0 1 1 0 0;0 1 1 0 0 0;0 1 1 0 1 1;0 0 1 1 1 1] + origin(1,2);
z=[0 0 0 0 0 1;0 0 0 0 0 1;1 1 1 1 0 1;1 1 1 1 0 1] + origin(1,3);
for i=1:6
    h=patch(x(:,i),y(:,i),z(:,i),color);
    set(h,'edgecolor','b')
end

end