function c_of_mass = centerofmass_m(matloc,gene)
%stable = 1 is stable
%stable = 0 unstable
%check that the projection of the center of mass falls on the base (minus
%some length epsilon
%normalize weight to one
m=length(gene);
nElements=1;
for i = 1:m
    if gene(i)==1
        nElements=nElements+1;
    end 
end

n=size(matloc,1);
cubeweight=1;

[x , y , z] = find(matloc);
c_of_mass=[sum(x) sum(y) sum(z)]./nElements

end