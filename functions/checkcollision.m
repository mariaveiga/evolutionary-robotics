function [allright] = checkcollision(bdir,startVec)

allright = 1;
length = 1; %will store the total amount of building blocks, the 1 is for the block at the origin
for l=1:size(startVec,1)
    length = length + size(bdir{l},1);
end
locVec = zeros(length,3);
iter = 1;
locVec(iter,:) = startVec(iter,:); %again this is for the block at the origin

for i=1:size(startVec,1) %for every branch
    
    coordVec = startVec(i,:);
    for j=1:size(bdir{i},1) %for every element of a specific branch
        
        iter = iter + 1;
        coordVec = coordVec + bdir{i}(j,:);
        locVec(iter,:) = coordVec;
        for k=1:iter-1 %check collision for a specific element
            
            if locVec(iter,:) == locVec(k,:) %collision happens
                allright = 0;
                return;
            end
            
        end
        
    end
    
end

end