%Rules -- structure element rule(x).before is string to be replaced for the xth rule.
%      -- structure element rule(x).after is the replacement string for the xth rule. 
function gene = encodeLsystem()

rule(1).before = '1';
rule(1).after = '121';

rule(2).before = '2';
rule(2).after = '131';%  

rule(3).before = '3';
rule(3).after = '141';

rule(4).before = '4';
rule(4).after = '151';

rule(5).before= '5';
rule(5).after='111';

%get the number of rules
nRules = length(rule);

%starting seed
axiom = '1';

%number of reps
nReps = 5;

for i=1:nReps
    
    %one character/cell, with indexes the same as original axiom string
    axiomINcells = cellstr(axiom');
    
    for j=1:nRules
        %Find the vector of indexes of each 'before' string for each rule
        hit = strfind(axiom, rule(j).before);
        if (length(hit)>=1)
		    %set all of the hits to the new string
			%This does not vectorize
            for k=hit
                axiomINcells{k} = rule(j).after;
            end
        end
    end

    rule = mutateRule(rule); %mutate the rule every iteration
    
    %now convert individual cells back to a string for the next repetition
    axiom=[];
    for j=1:length(axiomINcells)
        axiom = [axiom, axiomINcells{j}];
    end
end

%concatenate axiom into gene
gene = [];
for i=1:length(axiom)
  gene = [gene,' ',axiom(i)];
end

gene=str2num(gene);
gene=gene';