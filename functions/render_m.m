function render_m(organism,target,obstacles)
    
    figure
    axis xy
    axis on
    axis equal
    view(3);

    create_cube(organism.coordVec(1,:),[0.1 0.1 0.1]);
    
    for i=2:size(organism.coordVec,1)
        create_cube(organism.coordVec(i,:),[0.7 0.7 0.7]);
    end

%     for i=1:size(obstacles,1)
%         create_cube(obstacles(i,:),[1 0 0]); %obstacles are red
%     end

    create_cube(target,[0.4 0.4 0.4]);
    
        
end