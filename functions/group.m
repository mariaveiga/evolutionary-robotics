function [minCent, minIndex] = group (coordVec)
% INPUT:
% coordVec - matrix containing coordinates of all the cubes
% OUTPUT
% minCent - centroids
% minIndex - indices that indicate which cube belongs to which centroid


maxSize = 6;
max_iters = 15;
result = struct('K', {}, 'minCent', {}, 'minIndex', {});
numIter = 50;

for var = 1:numIter
    control = 1;
    iter2 = 0;
    iter1 = 0;
    K = round(size(coordVec,1)/(maxSize+1));
    
    while control
        rand_index = randperm(size(coordVec,1));
        initial_cent = coordVec(rand_index(1:K), :);
        [cent, cluster_index] = kMeans(coordVec, initial_cent, max_iters);
        
        % Check if there is more cubes than max defined
        % Check if more than 20 time if yes then increase number of
        % clusters
        if iter2 > 20
            K = K + 1;
            iter2 = 0;
            continue;
        end
        sum = zeros(K,1);
        for i = 1:K
            for j = 1:size(cluster_index,1)
                if cluster_index(j) == i
                    sum(i) = sum(i) + 1;
                end
            end
        end
        if max(sum)>maxSize
            iter2 = iter2 + 1;
            continue;
        end

        % Check building blocks
        flag = 0;
        for k = 1:K
            cubes = find(cluster_index == k);
            maxX = coordVec(cubes(1),1);
            maxY = coordVec(cubes(1),2);
            minX = coordVec(cubes(1),1);
            minY = coordVec(cubes(1),2);
            for i = 1:size(cubes,1)
                if coordVec(cubes(i),1)> maxX
                    maxX = coordVec(cubes(i),1);
                end
                if coordVec(cubes(i),1) < minX
                    minX = coordVec(cubes(i),1);
                end
                if coordVec(cubes(i),2)> maxY
                    maxY = coordVec(cubes(i),2);
                end
                if coordVec(cubes(i),2) < minY
                    minY = coordVec(cubes(i),2);
                end
            end
            
            if sum(k)==3 && (abs(maxX-minX) ~= 0 && abs(maxY-minY) ~= 0)
                flag = 1;
                break;
            end
            
            if sum(k)==4 && (maxX-minX ~= 1 || maxY-minY ~= 1)
                flag = 1;
                break;
            end
            
            if sum(k)==5
                flag = 1;
                break;
            end
            
            if sum(k)==6 && ((maxX-minX ~= 1 || maxY-minY ~= 2) && (maxX-minX ~= 2 || maxY-minY ~= 1))
                flag = 1;
                break;
            end
        end
        
        if iter1 > 20
            iter1 = 0;
            iter2 = 0;
            K = K+1;
            continue;
        end
        if flag
            iter2 = 0;
            iter1 = iter1 +1;
            continue;
        end
        
        
        % Define which one to use
        control = 0;
        result(var).K = K;
        result(var).minCent = cent;
        result(var).minIndex = cluster_index;
        if var == 1
            minK = K;
            minCent = cent;
            minIndex = cluster_index;
        end
        if K < minK
            minK = K;
            minCent = cent;
            minIndex = cluster_index;
        end
        disp(K);
    end
end
end

function [cent, index] = kMeans(coordVec, init_cent, max_iter)

[m, ~] = size(coordVec);
K = size(init_cent, 1);
cent = init_cent;
index = zeros(m, 1);

for i=1:max_iter
    index = findClosestCent(coordVec, cent);
    cent = computeCent(coordVec, index, K);
end

end


function index = findClosestCent(coordVec, cent)

K = size(cent, 1);
index = zeros(size(coordVec,1), 1);
d = zeros(K,1);
m = size(coordVec,1);

for i = 1:m
    for k = 1:K
        d(k) = sum(abs(cent(k,:)-coordVec(i,:)).^2);
    end
    [~, index(i)] = min(d);
end

end


function cent = computeCent(coordVec, index, K)

[m, n] = size(coordVec);
cent = zeros(K, n);
nEl=zeros(K,1);

for i = 1:m
    for k = 1:K
        if index(i) == k
            cent(k,:) = cent(k,:)+coordVec(i,:);
            nEl(k) = nEl(k)+1;
        end
    end
end

for k = 1:K
    cent(k,:)=cent(k,:)/nEl(k);
end

end
