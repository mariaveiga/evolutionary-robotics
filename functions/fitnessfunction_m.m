function [fitness] = fitnessfunction_m(organism,destination,useDeflection,useStress,useCubes,useConnections)

%startVec = organism.startVec;
%parentVec = organism.parentVec;
%bdir = organism.bdir;
maxStress = organism.maxStress;
maxDisplacement = organism.maxDisplacement;
numCon = organism.numCon;
% UPDATE 11.7. added displacement

%nBranch = size(parentVec,1);

nTarget = size(destination,1); % amount of destination points

%nTot = sum(cellfun('size',bdir,1)) + 1; % total amount of elements. the
                                        % plus one accounts for  the
                                        % element at the origin
                                        
% locVec = zeros(nTot,3);         % location vector of each element
% locVec(1,:) = startVec(1,:);    % element at the origin
% index = 2;
% for branch = 1:nBranch
%     coordVec = startVec(branch,:);
%     for element = 1:size(bdir{branch},1)
%         coordVec = coordVec + bdir{branch}(element,:);
%         locVec(index,:) = coordVec;
%         index = index + 1;
%     end
% end
% 


n=length(organism.gene);
% nElements=0;
% for i=1:n
%    if organism.gene(i)==1
%    nElements=nElements+1;
%    end
% end
nElements=size(organism.coordVec,1);

% coordVec = zeros(nElements,3);
% m=size(organism.matloc,1);
% l=1; %no 3D
% k=1;
% for i=1:m
%    for j=1:m
% %       for l=1:m
%        if organism.matloc(i,j,l)~= 0
%           coordVec(k,:)= [ l j m-i];
%           k=k+1;
%        end
% %   coordVec(j,:)=matLoc(
% %       end
%    end
% end


distVec = zeros(nElements,nTarget);  % distance between a specific element and a
                                % specific target point
for target = 1:nTarget
    for element = 1:nElements
%         nElements
%         element
%         norm(organism.coordVec(element,:))
        distVec(element,target) = norm(organism.coordVec(element,:)-destination(target,:));
    end
end

minimalDistance = zeros(1,nTarget); % the minimal distance between an
                                    % element and a specific destination
                                    % point
relativeError = zeros(1,nTarget); % the relative error for each target
for target = 1:nTarget
    [~,minIndex] = min(distVec(:,target),[],1);
%     n
%     nTarget
%     nElements
%     distVec
%     minIndex
    minimalDistance(1,target) = distVec(minIndex,target);
    relativeError(1,target) = minimalDistance(1,target)/norm(destination(target,:));
end

criticalStress = 1.2e5; % [N/m^2]
% UPDATE 8.7. changed critStress from 1.2e6 to 1.2e5 

stressRatio = maxStress/criticalStress; % the less stress the organism
% experiences, the higher the fitness is

distanceFactor = 1e2;

%number of operations without adding a cube (work)
%should this be penalised?
nOperations = length(organism.gene)-nElements;
nOperationsWeight=0.1;

%distance of center of mass to the boundary of the base (the further the
%better)
%calculate minimum distance and give priority to when this distance is
%largest

cubeweight=1;

c_of_mass=[sum(organism.coordVec(:,1)) sum(organism.coordVec(:,2)) sum(organism.coordVec(:,3))]./nElements; %can check against matLoc
projection=[c_of_mass(1) c_of_mass(2)];
i=1;
while (organism.coordVec(1,3)==organism.coordVec(i,3))
    base(i,:)=organism.coordVec(i,:);
    i=i+1;
end
base
%base=organism.locMat(:,:,1);
%check if the projection falls inside the base (up to an epsilon)
%maybe include dimensions of the cube to be closer to real world?
%[bx1,by1]=find(base,1,'first');
%[bx2,by2]=find(base,1,'last');
%[bx,by]=find(base);
acceptablebase=[5 5];
basesize=size(base,1);
b1=base(1,1:2);
%[bx2,by2]=base(basesize,:);
%check if falls inside DOF (URGH) (we have to check for EACH coordinate,
%not both at the same time, when we go to 3D WHAT A PAIN!!!)
    if b1(1)<=acceptablebase(1) && b1(2)<=acceptablebase(2)
        bx1=b1(1);
        by1=b1(2);
    else
        bx1(1)=acceptablebase(1);
        by1(2)=acceptablebase(2);
    end
    k=basesize;
    while(1)
        if base(k,1)<=acceptablebase(1) && base(k,2)<=acceptablebase(2) %take the base which is the largest but still part of the acceptablebase!!! ARGH
            bx2=base(k,1);
            by2=base(k,2);
            break;
        else
            k=k-1;
        end
    end
    
    %calculate distance
    q1 = sqrt((bx1-projection(1))^2+((by1-projection(2))^2));
    q2 = sqrt((bx2-projection(1))^2+((by2-projection(2))^2));
    qq=min(q1,q2);
    
tumblingWeight=0.1;
%stable=0;
%    if (re(1)>=bx1(1) && re(2)>=by1(1)) && (re(1)<=bx2(1) && re(2)<=by2(1))
%        stable=1;
%    end



if useCubes
    weightFactor = 0.01; 
else
    weightFactor = 0;
end
if useStress
    stabilityFactor = 5; %1e-01;
else
    stabilityFactor = 0;
end
if useDeflection
    displacementFactor = 750; %1e2;
else
    displacementFactor = 0;
end
if useConnections
    connectionFactor = 0.01;
else
    connectionFactor = 0;
end

%accounts for the relative error and punishes high numbers of elements
fitness = distanceFactor*(1-sum(relativeError,2)/nTarget) - weightFactor*nElements - stabilityFactor*stressRatio - displacementFactor*maxDisplacement + connectionFactor*numCon + tumblingWeight*qq;

end