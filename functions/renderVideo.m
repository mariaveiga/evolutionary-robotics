function renderVideo(organism,destination,useObstacles,obstacles)
    
    figure
    axis xy
    axis off
    axis equal
    view(3);
    framesz = [1 1 200 140];
    
    startVec = organism.startVec;
    bdir = organism.bdir;
    
    %the first cube is located at the origin and black
    % -> create_cube(location,color)
    create_cube(startVec(1,:),[0.1 0.1 0.1]);
    for d = 1:size(destination,1)
        create_cube(destination(d,:),[1 0 0]);
    end
    if useObstacles
        for o = 1:size(obstacles,1)
            create_cube(obstacles(o,:),[0.1 0.3 0.3]);
        end
    end
    for i=1:size(bdir,2) %for every branch
        coordVec = startVec(i,:);
        for j=1:size(bdir{i},1) %for every element of a specific branch
            coordVec = coordVec + bdir{i}(j,:);
            create_cube(coordVec,[0.7 0.7 0.7]);
        end
    end
    set(gcf,'Position',framesz,'Visible','Off');
end