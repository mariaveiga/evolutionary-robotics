function organism = build_m(gene,obstacles,target)
%receives a vector with several instructions
%assembles and checks for stability every n instructions
%generate the organism and check for obstacles on the fly
useObstacles=1;
%Funcionality:
n=max(find(gene,1,'last'));
%n = length(gene);
nCube=0;

%find how many elements are used in the structure
for i = 1:n
    if gene(i)==1
    nCube=nCube+1;
    end
end

maxStress = 0; % maximal stress in the structure
maxDisplacement = 0;
num_con = 0;


%STABILITY QUICK CHECK
param = paramst();

%dir = zeros(nCube-1,3); % stores direction vectors
%xCoord = n+1;
%yCoord = n;
%zCoord = nCube;

% locMat describes the location of each cube. 1 = space occupied, 0 = space
% free. it is used to check and prevent collision. this method is faster
% than using the location vectors of each cube to check if they overlap,
% because at this stage, there are just a few elements.
%locMat = zeros(nCube*2+1,nCube*2+1,nCube*2+1); 
%collision?
%in y direction we can't go below 0...
%not really tho! 
%locMat = zeros(n*2+1,n*2+1);
%we can reduce this matrix, actually


locMat = zeros(2*n+1,2*n+1,2*n+1);

locMat(n+1,n+1,n+1) = 1; %initial cube (x,y) is situated in the center of the matrix
iter = 1;
flag = 0;

% the starting cube is already predefined, so there has to be added one
% less cube.
control = 0;
j=2;

%operations are as follow: add (1), shift right (y direction) (2), shift left(3) and
%rotate 90� cc (4), rotate 90� anticc (5)
%shift along x direction positive %(6), shift along x direction negative
%(7)
%(where's the axis of rotation (initial cube?))
%Rules:can't shift twice
%      can't start with a shift.
% (y,x)

elem=1;
xCoord=n+1;
yCoord=n+1;
zCoord=n+1;
while j <= n 
    if gene(j) == 1 %add a cube
       elem=elem+1;
       %UPDATE: Establish where the base is to know the ground level and to add in
       %the cube (to the ground)
       [bx,by]=find(locMat(:,:,zCoord));
       base = [bx,by];
       sortrows(base,1);
       o=size(base,1);
       yCoord=detheight(locMat(:,xCoord,zCoord));
       if yCoord==-1
          yCoord=base(o,1); 
       end
       if locMat(yCoord,xCoord,zCoord)>nCube
            disp('Collisions');
            break;
       end
       %disp('Add a cube');
       locMat(yCoord,xCoord,zCoord)=locMat(yCoord,xCoord,zCoord)+elem;
%        allright=0; maxStress=0; maxDisplacement=0; num_con =0;
        [allright,maxStress,maxDisplacement,num_con] = stabilityquick(gene,locMat,target,useObstacles,obstacles);

        if allright ~= 1
             break;
        end
   elseif gene(j) == 2
       %disp('Shift right');
       xCoord=xCoord+1; %for ycoord we have to find the "height" of the structure on that column
       %yCoord=detheight(locMat(:,xCoord));
%        if allright ~= 1
%             break;
%        end
   elseif gene(j) == 3
       %disp('Shift left');
       xCoord=xCoord-1; %for ycoord we have to find the "height" of the structure on that column
       %yCoord=detheight(locMat(:,xCoord));
       
   elseif gene(j) == 4 %go back to the center every time we rotate... does this make sense?
                       %the problem if we don't do this is for instance:
                       % 0 0 0 0
                       % 0 0 0 0
                       % x x 0 0
                       % 0 x 0 0 and apply rotation of 90�,
                       % xcoord=1,ycoord=2
                       % we have 
                       % 0 0 0 0
                       % 0 0 0 0
                       % 0 0 x x
                       % 0 0 x 0 add a cube, yields a structure that is
                       % disconnected... sooooooooooooo. 
                       % 
       %disp('Rotated 90�, to be implemented');
       locMat(:,:,zCoord)=rot90(locMat(:,:,zCoord));
       xCoordold=xCoord; yCoordold=yCoord;
       xCoord=yCoordold;
       yCoord=2*n+2-xCoordold;
%        [allright,maxStress,maxDisplacement,num_con] = stabilityquick(gene,locMat,target,useObstacles,obstacles);
%        %stress might not be that important, check if tumbles is probably
%        %more important
%         if allright ~= 1
%              break;
%         end
   elseif gene(j) == 5
       locMat(:,:,zCoord)=rot90(locMat(:,:,zCoord),-1);
       %disp('Rotated 90� cc, to be implemented');
%        xCoord=2*n+2-xCoord;
%        yCoord=n+1;
%     elseif gene(j) ==6
%        zCoord=zCoord+1;
       xCoordold=xCoord; yCoordold=yCoord;
       xCoord=2*n+2-yCoordold;
       yCoord=xCoordold;
%        [allright,maxStress,maxDisplacement,num_con] = stabilityquick(gene,locMat,target,useObstacles,obstacles);
%        %stress might not be that important, check if tumbles is probably
%        %more important
%         if allright ~= 1
%              break;
%         end
   elseif gene(j) == 0
       %disp('Finished');
       %continue;
       break;
   else
       %disp('Unknown instruction');
    end 
   j=j+1;
end
   
outx =(n+1-nCube):(n+1+nCube);
outy =(n+1-nCube):(n+1+nCube);
outz=(n+1-nCube):(n+1+nCube);
maxStress = 0; % maximal stress in the structure
maxDisplacement = 0;
num_con = 0;

locMat=locMat(outx,outy,outz);

m=size(locMat,1);

[x0,y0,v]=find(locMat);
[y,z,x] = ind2sub(size(locMat),find(locMat));
%coordVec1 = [v, x, y , z]
coordVec1=[v,x,z,m-y]; %removed the m-y minus
%coordVec = sortrows(coordVec1,1);
coordVec = sortrows(coordVec1,[4,2]); %order by height (ascending) and by x (x doesn't change anything derp), so it should actually be y descending...
coordVec = coordVec(:,2:4); %shift coordinates to origin, which is the lowest point in the z direction
                           %and for convinience, 
%coordVec = coordVec(:,:) - repmat([0 coordVec(1,2:4)],size(coordVec,1),1);
coordVec = coordVec(:,:) - repmat(coordVec(1,:),size(coordVec,1),1);

organism = struct('gene', gene, 'coordVec', coordVec, 'maxStress', maxStress,'maxDisplacement', maxDisplacement,'numCon', num_con);

%coordVec1 = sortrows(coordVec,1)

%generate construction video%
% for i=1:size(coordVec,1)
%     stepwiserender_m(coordVec1(1:i,:));
% end
% 
end