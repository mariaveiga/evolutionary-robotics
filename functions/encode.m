function encoding = encode(nCubes,maxOps,useObstacles,obstacles)
%receives a number of cubes and creates a sequence of maxOps that should yield
%that number of cubes

%operations are as follow: add (1), shift right (2), shift left(3) and
%rotate 90� cc (4), rotate 90� anticc (5) (where's the axis of rotation)
%Rules:can't shift twice
%      can't start with a shift.
%      bug if I shift and rotate

encoding=zeros(maxOps,1);
encoding(1)=1; %start with a cube
cubes=1;
i=2;
while i <=maxOps
%while (cubes<=nCubes)
%for i=2:maxOps
%   operation = randi(6);
   operation = randi(5);
   switch operation
       case {1}
           encoding(i)=1;
           cubes=cubes+1;
       case {2} %shift right
           if encoding(i-1)==2 || encoding(i-1)==4 || encoding(i-1)==5 || encoding(i-1)==6
               i=i-1;
               continue;
           end
            encoding(i)=2;
       case {3} %shift left
           if encoding(i-1)==3 || encoding(i-1)==4 || encoding(i-1)==5
           i=i-1;
           continue;
           end
           encoding(i)=3;
       case {4}    
           if encoding(i-1)==2 || encoding(i-1)==3 %not sure
                i=i-1;
               continue;
           end
           encoding(i)=4;
       case {5}
           if encoding(i-1)==2 || encoding(i-1)==3 %not sure
           i=i-1;
               continue;
           end
           encoding(i)=5;
%        case {6}
%            if encoding(i-1)==2 || encoding(i-1)==4 || encoding(i-1)==5 ...
%                    || encoding(i-1)==6
%            i=i-1;
%            continue;
%            end
%             encoding(i)=6;
       otherwise
           disp('Unknown command');
   end
   if (cubes>=nCubes)
       break;
   end
   i=i+1;
end
%end
%end



end