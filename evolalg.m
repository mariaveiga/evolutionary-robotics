% Evolutionary Algorithm
% -----------------------------
% Author: Jeremias Seitz
% Modified: Tonci Novkovic
% -----------------------------

% INITIALIZATION
clear variables;
close all;
clc

addpath functions
addpath Truss_Analysis
addpath movieFrames

% Starting parameters:
maxGen = 50;                                   % maximum number of generations
nCubeMin = 10;                                  % minimum number of starting cubes
nCubeMax = 50;                                  % maximum number of starting cubes
nPopulation = 10;                               % size of the population
nElite = 1;                                     % size of the elite
mutationPercent = 0.6;                          % percentage of mutations wrt cross overs

nMutate = round(mutationPercent*(nPopulation-nElite));      % number of mutations
nCrossover = nPopulation - nElite - nMutate;    % number of cross overs

% Targets:
destination = [11 11 5];        % coordinates of the destination point

% External load (just one for now):
externalLoad = [0 0 0];                 % summation of all external forces
externalLoadPosition = destination;     % position of applied force

% Obstacle postions:
% obstacles = [1 0 0; -1 0 0; 0 1 0; 0 -1 0; 1 1 0; -1 -1 0; 1 -1 0; -1 1 0;
%             1 0 1; -1 0 1; 0 1 1; 0 -1 1; 1 1 1; -1 -1 1; 1 -1 1; -1 1 1;
%             1 0 2; -1 0 2; 0 1 2; 0 -1 2; 1 1 2; -1 -1 2; 1 -1 2; -1 1 2;
%             1 0 -1; -1 0 -1; 0 1 -1; 0 -1 -1; 1 1 -1; -1 -1 -1; 1 -1 -1; 
%             -1 1 -1; 0 0 -1]; % around base

% obstacles =    [0 0 -1; 2 -2 -1; 2 -2 0; 2 -2 1; 2 -2 2; 2 -2 3;  
%                 2 -1 -1; 2 -1 0; 2 -1 1; 2 -1 2; 2 -1 3; 
%                 1 -2 -1; 1 -2 0; 1 -2 1; 1 -2 2; 1 -2 3;
%                 2 0 -1; 2 0 0; 2 0 1; 2 0 2; 2 0 3; 
%                 0 -2 -1; 0 -2 0; 0 -2 1; 0 -2 2; 0 -2 3;]; % wall

% k=1;
% obstacles = zeros(3403,3);
% for i = -20:1:20
%     for j = -20:1:20
%         obstacles(k,:) = [i -1 j];
%         obstacles(k+1,:) = [i 1 j];
%         k = k+2;
%     end
% end
% for i = -20:1:20
%     obstacles(k,1) = i;
%     obstacles(k,3) = -1;
%     k = k + 1;
% end       % plane

obstacles = [0 0 -1];

% Number of organisms to render in last generation
desired = 1;

% Options:
useProfiler = 0;     % 1 - use profiler, 0 - don't
useMovieMake = 0;    % 1 - make movie, 0 - don't
useParallel = 0;     % 1 - use parallel, 0 - don't
% if parallel is used then algorithm can't check for existing 

% Algorithm properties:
useStability = 1;    % 1 - stability gets checked, 0 - doesn't
useObstacles = 1;    % 1 - use obstacles, 0 - don't

% Fitness function parameters:
useDeflection = 1;  
useStress = 1;
useCubes = 1;
useConnections = 1;

organism = struct('parentVec', {}, 'startVec', {}, 'bdir', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
elite = struct('parentVec', {}, 'startVec', {}, 'bdir', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
mutate = struct('parentVec', {}, 'startVec', {}, 'bdir', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});
crossover = struct('parentVec', {}, 'startVec', {}, 'bdir', {}, 'maxStress', {},'maxDisplacement', {},'numCon', {});

if useProfiler
    profile on
end

if useParallel
    matlabpool local
end

%pdf_mut_mix = pdf('bino',[0,0.1,0.2]);
pdf_mut_mix = pdf(nPopulation);
% probability density function used to calculate the probability of
% choosing a certain organism for mutation and cross over (first organism 
% has the highest probability)

% INITIAL POPULATION
generation = 1;                 % current generation
pStart = tic;
if useParallel
    parfor i=1:nPopulation
        r = rand;
        pdf_init = ones(1,nCubeMax-nCubeMin+1)*1/(nCubeMax-nCubeMin+1);
        nCube = find(r <= cumsum(pdf_init),1)+nCubeMin-1;
        organism(generation,i) = create(nCube,useObstacles,obstacles);
        [allright,maxStress,maxDisplacement,num_con] = checkstability(organism(generation,i).bdir,organism(generation,i).startVec,externalLoad,externalLoadPosition,useObstacles,obstacles);
        organism(generation,i).maxStress = maxStress;
        organism(generation,i).maxDisplacement = maxDisplacement;
        organism(generation,i).numCon = num_con;
        while allright ~= 1
            r = rand;
            pdf_init = ones(1,nCubeMax-nCubeMin+1)*1/(nCubeMax-nCubeMin+1);
            nCube = find(r <= cumsum(pdf_init),1)+nCubeMin-1;
            organism(generation,i) = create(nCube,useObstacles,obstacles);
            [allright,maxStress,maxDisplacement,num_con] = checkstability(organism(generation,i).bdir,organism(generation,i).startVec,externalLoad,externalLoadPosition,useObstacles,obstacles);
            organism(generation,i).maxStress = maxStress;
            organism(generation,i).maxDisplacement = maxDisplacement;
            organism(generation,i).numCon = num_con;
        end
    end
else
    i = 1;
    while i <= nPopulation
        r = rand;
        pdf_init = ones(1,nCubeMax-nCubeMin+1)*1/(nCubeMax-nCubeMin+1);
        nCube = find(r <= cumsum(pdf_init),1)+nCubeMin-1;
        organism(generation,i) = create(nCube,useObstacles,obstacles);
        [allright,maxStress,maxDisplacement,num_con] = checkstability(organism(generation,i).bdir,organism(generation,i).startVec,externalLoad,externalLoadPosition,useObstacles,obstacles);
        organism(generation,i).maxStress = maxStress;
        organism(generation,i).maxDisplacement = maxDisplacement;
        organism(generation,i).numCon = num_con;
        if checkexisting(organism(generation,:),i)
            continue;
        end
        if allright ~= 1
            continue;
        end
        i = i+1;
    end
end
organism(generation,:) = checkfitness(nPopulation,organism(generation,:),destination,useDeflection,useStress,useCubes,useConnections);
f = fitnessfunction(organism(generation,1),destination,useDeflection,useStress,useCubes,useConnections);
pEnd = toc(pStart);
disp([num2str(generation),'/',num2str(maxGen),'   ',num2str(f,'%7.4f'), '    Time: ', num2str(pEnd,'%6.2f')]);

generation = generation + 1;
iTime = 0;
mTime = 0;
cTime = 0;

% ELITE, MUTATION, CROSS OVER
while f < 150 && generation<=maxGen
    iStart = tic;
    prev_gen(1:nPopulation) = organism(generation-1,:);
    
    % externalLoad = generation .* 0.03 .* [-1 1 -1]; 
    
    % Elite
    elite(1:nElite) = prev_gen(1:nElite);
    
    % Mutation
    nMutation = nMutate;
    mStart = tic;
    if useParallel
        parfor i = 1:nMutation
            mut_rand = rand;
            mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
            mutate(i) = mutation(prev_gen(mutOrg),useStability,externalLoad,externalLoadPosition,useObstacles,obstacles);
        end
    else
        i = 1;
        while i <= nMutation
            mut_rand = rand;
            mutOrg = find(mut_rand <= cumsum(pdf_mut_mix),1);
            mutate(i) = mutation(prev_gen(mutOrg),useStability,externalLoad,externalLoadPosition,useObstacles,obstacles);
            if checkexisting(mutate,i)
                continue;
            end
            i = i+1;
        end
    end
    mEnd = toc(mStart);
    mTime = mTime + mEnd;
    
    % Cross over
    cStart = tic;
    if useParallel
        parfor j = 1:nCrossover
            A_rand = rand;
            B_rand = rand;
            objA = find(A_rand <= cumsum(pdf_mut_mix),1);        % evolving
            objB = find(B_rand <= cumsum(pdf_mut_mix),1);        % providing elemets to A
            crossover(j) = mix(prev_gen(objA),prev_gen(objB),useStability,externalLoad,externalLoadPosition,useObstacles,obstacles);
        end
    else
        j = 1;
        while j <= nCrossover
            A_rand = rand;
            B_rand = rand;
            objA = find(A_rand <= cumsum(pdf_mut_mix),1);        % evolving
            objB = find(B_rand <= cumsum(pdf_mut_mix),1);        % providing elemets to A
            crossover(j) = mix(prev_gen(objA),prev_gen(objB),useStability,externalLoad,externalLoadPosition,useObstacles,obstacles);
            if checkexisting(crossover,j)
                continue;
            end
            j = j+1;
        end
    end
    cEnd = toc(cStart);
    cTime = cTime + cEnd;
        
    new_generation = [elite, mutate, crossover];
    new_generation = checkfitness(nPopulation,new_generation,destination,useDeflection,useStress,useCubes,useConnections);
    organism(generation,:) = new_generation;
    f = fitnessfunction(organism(generation,1),destination,useDeflection,useStress,useCubes,useConnections);
        
    iEnd = toc(iStart);
    iTime = iTime + iEnd;
    disp([num2str(generation),'/',num2str(maxGen),'   ',num2str(f,'%7.4f'),'    Mutation: ',num2str(mEnd,'%6.2f'),' Crossover: ',num2str(cEnd,'%6.2f'),' Iteration: ',num2str(iEnd,'%6.2f')]);
    
    generation = generation + 1;
    
    if generation > maxGen
        break;
    end
    
end
disp (['Total time: ',num2str(iTime,'%6.2f'),'   Mutation: ',num2str(mTime,'%6.2f'),'   Cross over: ',num2str(cTime,'%6.2f')]);



for i = 1:desired
    render(organism(generation-1,i));
end

if useMovieMake
    makeMovie(organism,generation-1,destination,useObstacles,obstacles);
end

if useParallel
    matlabpool CLOSE;
end

if(useProfiler)
    profile off
    profile viewer
end